﻿using AvioKarte.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AvioKarte.Model;
using AvioKarte.Database;

namespace AvioKarte.Utility
{
    public class Tool
    {
        public Korisnik loggedUser { get; set; }
        public Karta kartaDoOdredista { get; set; }

        public Karta kartaTransfer { get; set; }

        private Tool()
        {
        }

        private static Tool _instance = null;

        public static Tool Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Tool();
                }
                return _instance;
            }

        }

       

        public bool proveriPostojeceSifreAerodroma(string sifra)
        {
            foreach(Aerodrom aerodrom in Data.Instance.Aerodromi)
            {
                if (aerodrom.Sifra.ToUpper().Equals(sifra.ToUpper()))
                {
                    return true;
                    
                }
            }
            return false;
        }

        public bool proveriPostojeceKorisnickoIme(string korisnickoIme)
        {
            foreach (Korisnik korisnik in Data.Instance.Korisnici)
            {
                if (korisnik.KorisnickoIme.ToUpper().Equals(korisnickoIme.ToUpper()))
                {
                    return true;

                }
            }
            return false;
        }

        public bool proveriPostojeceSifreAviokompanije(string sifra)
        {
            foreach (Aviokompanija aviokompanija in Data.Instance.Aviokompanije)
            {
                if (aviokompanija.Sifra.ToUpper().Equals(sifra.ToUpper()))
                {
                    return true;

                }
            }
            return false;
        }

        public int indexAerodroma(String sifraAerodroma)
        {
            int index = -1;

            for (int i = 0; i < Data.Instance.Aerodromi.Count; i++)
            {
                if (Data.Instance.Aerodromi[i].Sifra.Equals(sifraAerodroma))
                {
                    index = i;
                    break;
                }
            }

            return index;
        }

        public int indexAviokompanije(String sifraAviokompanije)
        {
            int index = -1;

            for (int i = 0; i < Data.Instance.Aviokompanije.Count; i++)
            {
                if (Data.Instance.Aviokompanije[i].Sifra.Equals(sifraAviokompanije))
                {
                    index = i;
                    break;
                }
            }

            return index;
        }

        public int indexKorisnika(String korisnickoIme)
        {
            int index = -1;

            for (int i = 0; i < Data.Instance.Korisnici.Count; i++)
            {
                if (Data.Instance.Korisnici[i].KorisnickoIme.Equals(korisnickoIme))
                {
                    index = i;
                    break;
                }
            }

            return index;
        }
        public int indexLeta(String brojLeta)
        {
            int index = -1;

            for (int i = 0; i < Data.Instance.Letovi.Count; i++)
            {
                if (Data.Instance.Letovi[i].BrojLeta.Equals(brojLeta))
                {
                    index = i;
                    break;
                }
            }

            return index;
        }
        public int indexAviona(String sifraAviona)
        {
            int index = -1;

            for (int i = 0; i < Data.Instance.Avioni.Count; i++)
            {
                if (Data.Instance.Avioni[i].SifraAviona.Equals(sifraAviona))
                {
                    index = i;
                    break;
                }
            }

            return index;
        }


        public int indexKarte(int sifraKarte)
        {
            /*
            int index = -1;

            for (int i = 0; i < Data.Instance.Karte.Count; i++)
            {
                if (Data.Instance.Karte[i].SifraKarte.Equals(sifraKarte))
                {
                    index = i;
                    break;
                }
            }*/

            return 1;
           // return index;
        }
    }
}

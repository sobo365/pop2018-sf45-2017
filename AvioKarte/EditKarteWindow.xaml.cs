﻿using AvioKarte.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AvioKarte.Database;
using AvioKarte.Model;
using AvioKarte.Utility;

namespace AvioKarte
{
    /// <summary>
    /// Interaction logic for EditKarteWindow.xaml
    /// </summary>
    public partial class EditKarteWindow : Window
    {
        public enum Opcija { DODAVANJE, IZMENA};
        Karta karta;
        Opcija opcija;
        public EditKarteWindow(Karta karta, Opcija opcija)
        {
            InitializeComponent();
            this.DataContext = karta;
            this.karta = karta;
            this.opcija = opcija;

            
            cbKorisnik.ItemsSource = Data.Instance.Korisnici;
            cbLet.ItemsSource = Data.Instance.Letovi;
            cbKlasaSedista.ItemsSource = Enum.GetValues(typeof(EKlasaSedista)).Cast<EKlasaSedista>();
            
        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            
            if(opcija.Equals(Opcija.IZMENA))
            {
                /*
                int index = Tool.Instance.indexKarte(karta.SifraKarte);
                Data.Instance.Karte[index] = karta;
                this.Close();
                */
            }

            if (opcija.Equals(Opcija.DODAVANJE))
            {

                try
                {
                    int red = Int32.Parse(txtSedisteRed.Text);
                    string kolona = txtSedisteKol.Text;
                    int indexAviona = Tool.Instance.indexAviona(karta.Let.Avion.SifraAviona);

                    string sediste = red.ToString() + kolona;


                    EKlasaSedista klasaSedista = (EKlasaSedista)cbKlasaSedista.SelectedItem;
                    Let selektovaniLet = cbLet.SelectedItem as Let;



                    foreach (Karta k in Data.Instance.Karte)
                    {
                        if (klasaSedista.Equals(EKlasaSedista.BIZNIS))
                        {
                            foreach (Sediste s in selektovaniLet.Avion.ListaSedistaBiznis)
                            {
                                if (sediste.Equals(s.ToString()))
                                {
                                    if (k.Let.Avion == selektovaniLet.Avion && sediste.Equals(k.Sediste.ToString()) && klasaSedista.Equals(k.Sediste.KlasaSedista))
                                    {
                                        MessageBox.Show($"{sediste} je zauzeto!");
                                        karta.Sediste = null;
                                        break;
                                    }
                                    else
                                    {
                                        karta.Sediste = s;
                                        break;
                                    }
                                }
                            }
                        }

                        if (klasaSedista.Equals(EKlasaSedista.EKONOMSKA))
                        {
                            foreach (Sediste s in selektovaniLet.Avion.ListaSedistaEkonomska)
                            {
                                if (sediste.Equals(s.ToString()))
                                {
                                    if (k.Let.Avion == selektovaniLet.Avion && sediste.Equals(k.Sediste.ToString()) && klasaSedista.Equals(k.Sediste.KlasaSedista))
                                    {
                                        MessageBox.Show($"{sediste} je zauzeto!");
                                        karta.Sediste = null;
                                        break;
                                    }
                                    else
                                    {
                                        karta.Sediste = s;
                                        break;
                                    }
                                }
                            }
                        }
                    }


                    /*

                     if (klasaSedista.Equals(EKlasaSedista.BIZNIS))
                     {
                         foreach(Karta k in Data.Instance.Karte)
                         {
                             foreach(Sediste s in selektovaniLet.Avion.ListaSedistaBiznis)
                             {
                                 if(sediste.Equals(s.ToString()))
                                 {
                                     if(k.Let.Avion == selektovaniLet.Avion && sediste.Equals(k.Sediste.ToString()) && klasaSedista.Equals(k.Sediste.KlasaSedista))
                                     {
                                         MessageBox.Show($"{sediste} je zauzeto!");
                                         break;
                                     }
                                     else
                                     {
                                         karta.Sediste = s;
                                         break;
                                     }
                                 }
                             }
                         }
                     }
                     if (klasaSedista.Equals(EKlasaSedista.EKONOMSKA))
                     {
                         foreach (Karta k in Data.Instance.Karte)
                         {
                             foreach (Sediste s in selektovaniLet.Avion.ListaSedistaEkonomska)
                             {
                                 if (sediste.Equals(s.ToString()))
                                 {
                                     if (k.Let.Avion == selektovaniLet.Avion && sediste.Equals(k.Sediste.ToString()) && klasaSedista.Equals(k.Sediste.KlasaSedista))
                                     {
                                         MessageBox.Show($"{sediste} je zauzeto!");
                                         break;
                                     }
                                     else
                                     {

                                         karta.Sediste = s;
                                         break;
                                     }
                                 }
                             }
                         }
                     }
                     */


                   
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Pogresno uneto sediste");
                }

                if (karta.Sediste == null)
                {
                    MessageBox.Show("Pogresno uneto sediste!");
                }
                else
                {
                    if (karta.KlasaSedista.Equals(EKlasaSedista.BIZNIS))
                    {
                        karta.Cena = karta.Let.CenaKarte * 1.5;
                    }
                    else
                    {
                        karta.Cena = karta.Let.CenaKarte;
                    }

                    karta.populateEntities();
                    karta.Insert();
                    Data.Instance.UcitajKarteDB();
                    this.Close();
                }


            }

        }

        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void cbLet_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }
    }
}

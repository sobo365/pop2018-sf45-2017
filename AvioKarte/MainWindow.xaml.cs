﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AvioKarte.Pages;

namespace AvioKarte
{
    public partial class MainWindow : Window
    {
        

        public MainWindow()
        {
            
            InitializeComponent();
            Main.Content = new PutnikPocetnaPage();
        }

        private void btnPocetna_Click(object sender, RoutedEventArgs e)
        {
            Main.Content = new PutnikPocetnaPage();

        }

        private void btnLetovi_Click(object sender, RoutedEventArgs e)
        {
            Main.Content = new LetoviPage(Main);
        }

        private void btnPrijava_Click(object sender, RoutedEventArgs e)
        {
            Main.Content = new PrijavaPage(this);
        }

        private void btnIzlaz_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvioKarte.Model
{
    public class Let : INotifyPropertyChanged, ICloneable
    {
        public int Id { get; set; }
        private string brojLeta;
        public string BrojLeta {
            get { return brojLeta; }
            set
            {
                brojLeta = value;
                OnPropertyChanged("BrojLeta");
            }
        }
        private DateTime vremePolaska { get; set; }
        public DateTime VremePolaska {
            get { return vremePolaska; }
            set
            {
                vremePolaska = value;
                OnPropertyChanged("VremePolaska");
            }
        }
        private DateTime vremeDolaska;
        public DateTime VremeDolaska {
            get { return vremeDolaska; }
            set
            {
                vremeDolaska = value;
                OnPropertyChanged("VremeDolaska");
            }
        }
        private Aerodrom polaziste;
        public Aerodrom Polaziste {
            get { return polaziste; }
            set
            {
                polaziste = value;
                OnPropertyChanged("Polaziste");
            }
        }
        private Aerodrom odrediste;
        public Aerodrom Odrediste {
            get { return odrediste; }
            set
            {
                odrediste = value;
                OnPropertyChanged("Odrediste");
            }
        }
        private double cenaKarte;
        public double CenaKarte {
            get { return cenaKarte; }
            set
            {
                cenaKarte = value;
                OnPropertyChanged("CenaKarte");
            }
        }
        private Aviokompanija aviokompanija;
        public Aviokompanija Aviokompanija {
            get { return aviokompanija; }
            set
            {
                aviokompanija = value;
                OnPropertyChanged("Aviokompanija");
            }
        }
        private Avion avion;
        public Avion Avion {
            get { return avion; }
            set
            {
                avion = value;
                OnPropertyChanged("Avion");
            }

        }
        private string pilot;
        public string Pilot {
            get { return pilot; }
            set
            {
                pilot = value;
                OnPropertyChanged("Pilot");
            }
        }
        private bool active;
        public bool Active { get; set; }


        public Let(string brojLeta, DateTime vremePolaska, DateTime vremeDolaska, Aerodrom polaziste, Aerodrom odrediste, double cenaKarte, Aviokompanija aviokompanija, Avion avion, string pilot)
        {
            BrojLeta = brojLeta;
            VremePolaska = vremePolaska;
            VremeDolaska = vremeDolaska;
            Odrediste = odrediste;
            Polaziste = polaziste;
            CenaKarte = cenaKarte;
            Aviokompanija = aviokompanija;
            Avion = avion;
            Pilot = pilot;
            Active = true;
        }

        public Let()
        {
            BrojLeta = "";
            VremePolaska = new DateTime();
            VremeDolaska = new DateTime();
            Odrediste = new Aerodrom();
            Polaziste = new Aerodrom();
            CenaKarte = 0;
            Aviokompanija = new Aviokompanija();
            Active = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return Polaziste + " - \n" + Odrediste;
        }

        public object Clone()
        {
            Let let = new Let()
            {
                Id = Id,
                BrojLeta = BrojLeta,
                VremePolaska = VremePolaska,
                VremeDolaska = VremeDolaska,
                Odrediste = Odrediste,
                Polaziste = Polaziste,
                CenaKarte = CenaKarte,
                Aviokompanija = Aviokompanija,
                Avion = Avion,
                Pilot = Pilot,
                Active = Active
            };
            return let;
        }

        public void Insert()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Letovi(BrojLeta, VremePolaska, VremeDolaska, PolazniAerodromID, DolazniAerodromID, AvionID, CenaKarte, AviokompanijaID, Pilot, Active)
                                        values(@BrojLeta, @VremePolaska, @VremeDolaska, @PolazniAerodromID, @DolazniAerodromID, @AvionID, @CenaKarte, @AviokompanijaID, @Pilot, @Active)";

                command.Parameters.Add(new SqlParameter("@BrojLeta", this.BrojLeta));
                command.Parameters.Add(new SqlParameter("@VremePolaska", this.VremePolaska));
                command.Parameters.Add(new SqlParameter("@VremeDolaska", this.VremeDolaska));
                command.Parameters.Add(new SqlParameter("@PolazniAerodromID", this.polaziste.id));
                command.Parameters.Add(new SqlParameter("@DolazniAerodromID", this.odrediste.id));
                command.Parameters.Add(new SqlParameter("@AviokompanijaID", this.aviokompanija.Id));
                command.Parameters.Add(new SqlParameter("@AvionID", this.Avion.Id));
                command.Parameters.Add(new SqlParameter("@CenaKarte", this.cenaKarte));
                command.Parameters.Add(new SqlParameter("@Pilot", this.pilot));
                command.Parameters.Add(new SqlParameter("@Active", true));

                command.ExecuteNonQuery();
            }
        }

        public void Delete()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update letovi set Active = 0 where Id = @id";

                command.Parameters.Add(new SqlParameter("@id", this.Id));

                command.ExecuteNonQuery();
            }
        }

        public void Update()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update letovi set @BrojLeta = BrojLeta, @VremePolaska = VremePolaska, VremeDolaska = @VremeDolaska,
                                        PolazniAerodromID = @PolazniAerodromID, DolazniAerodromID = @DolazniAerodromID, AviokompanijaID = @AviokompanijaID,
                                        AvionID = @AvionID, CenaKarte = @CenaKarte, Pilot = @Pilot,Active = @Active where Id = @id";

                command.Parameters.Add(new SqlParameter("@id", this.Id));
                command.Parameters.Add(new SqlParameter("@BrojLeta", this.BrojLeta));
                command.Parameters.Add(new SqlParameter("@VremePolaska", this.VremePolaska));
                command.Parameters.Add(new SqlParameter("@VremeDolaska", this.VremeDolaska));
                command.Parameters.Add(new SqlParameter("@PolazniAerodromID", this.polaziste.id));
                command.Parameters.Add(new SqlParameter("@DolazniAerodromID", this.odrediste.id));
                command.Parameters.Add(new SqlParameter("@AviokompanijaID", this.aviokompanija.Id));
                command.Parameters.Add(new SqlParameter("@AvionID", this.Avion.Id));
                command.Parameters.Add(new SqlParameter("@CenaKarte", this.cenaKarte));
                command.Parameters.Add(new SqlParameter("@Pilot", this.pilot));
                command.Parameters.Add(new SqlParameter("@Active", true));

                command.ExecuteNonQuery();
            }
        }
    }
}

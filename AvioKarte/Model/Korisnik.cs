﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvioKarte.Model
{
    public class Korisnik : INotifyPropertyChanged, ICloneable
    {
        public int Id { get; set; }
        private string ime;
        public string Ime
        {
            get { return ime; }
            set
            {
                ime = value;
                OnPropertyChanged("Ime");
            }
        }
        private string prezime;
        public string Prezime
        {
            get { return prezime; }
            set
            {
                prezime = value;
                OnPropertyChanged("Prezime");
            }
        }
        private string lozinka;
        public string Lozinka {
            get { return lozinka; }
            set
            {
                lozinka = value;
                OnPropertyChanged("Lozinka");
            }
        }
        private string korisnickoIme;
        public string KorisnickoIme
        {
            get { return korisnickoIme; }
            set
            {
                korisnickoIme = value;
                OnPropertyChanged("KorisnickoIme");
            }
        }
        private EPol pol;
        public EPol Pol
        {
            get { return pol; }
            set
            {
                pol = value;
                OnPropertyChanged("Pol");
            }
        }
        private string adresa;
        public string Adresa
        {
            get{ return adresa; }
            set
            {
                adresa = value;
                OnPropertyChanged("Adresa");
            }
        }
        private ETipKorisnika tipKorisnika;
        public ETipKorisnika TipKorisnika {
            get { return tipKorisnika; }
            set
            {
                tipKorisnika = value;
                OnPropertyChanged("TipKorisnika");
            }
        }
        private bool active;
        public bool Active {
            get { return active; }
            set
            {
                active = value;
                OnPropertyChanged("Active");
            }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set
            {
                email = value;
                OnPropertyChanged("Email");
            }
        }

        public Korisnik(string Ime, String Prezime, String Lozinka, String KorisnickoIme, EPol Pol, String Adresa, ETipKorisnika TipKorisnika)
        {
            this.Ime = Ime;
            this.Prezime = Prezime;
            this.Lozinka = Lozinka;
            this.KorisnickoIme = KorisnickoIme;
            this.Pol = Pol;
            this.Adresa = Adresa;
            this.TipKorisnika = TipKorisnika;
            this.Active = true;
        }

        public Korisnik()
        {
            Id = 0;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return Ime + " " + Prezime ;
        }

        public object Clone()
        {
            Korisnik korisnik = new Korisnik
            {
                Id = this.Id,
                Ime = this.Ime,
                Prezime = this.Prezime,
                KorisnickoIme = this.KorisnickoIme,
                Lozinka = this.Lozinka,
                Pol = this.Pol, 
                Email = Email,
                TipKorisnika = this.TipKorisnika,
                Adresa = this.Adresa,
                Active = this.Active
            };
            return korisnik;
        }

        public void Insert()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Korisnici(Ime, Prezime, Lozinka, KorisnickoIme, Pol, Adresa, Email, TipKorisnika, Active)
                                        values(@Ime, @Prezime, @Lozinka, @KorisnickoIme, @Pol, @Adresa, @Email, @TipKorisnika, @Active)";

                command.Parameters.Add(new SqlParameter("@Ime", this.ime));
                command.Parameters.Add(new SqlParameter("@Prezime", this.prezime));
                command.Parameters.Add(new SqlParameter("@Lozinka", this.lozinka));
                command.Parameters.Add(new SqlParameter("@KorisnickoIme", this.korisnickoIme));
                command.Parameters.Add(new SqlParameter("@Pol", this.pol.ToString()));
                command.Parameters.Add(new SqlParameter("@Adresa", this.adresa));
                command.Parameters.Add(new SqlParameter("@Email", this.email));
                command.Parameters.Add(new SqlParameter("@TipKorisnika", this.tipKorisnika.ToString()));
                command.Parameters.Add(new SqlParameter("@Active", true));

                command.ExecuteNonQuery();
            }
        }

        public void Delete()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update korisnici set Active = 0 where Id = @id";

                command.Parameters.Add(new SqlParameter("@id", this.Id));

                command.ExecuteNonQuery();
            }
        }

        public void Update()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update korisnici set Ime = @Ime, Prezime = @Prezime, Lozinka = @Lozinka, KorisnickoIme = @KorisnickoIme,
                                       Pol = @Pol, Adresa = @Adresa, Email = @Email,TipKorisnika = @TipKorisnika, Active = @Active where Id = @id";

                command.Parameters.Add(new SqlParameter("@Ime", this.ime));
                command.Parameters.Add(new SqlParameter("@id", this.Id));
                command.Parameters.Add(new SqlParameter("@Prezime", this.prezime));
                command.Parameters.Add(new SqlParameter("@Lozinka", this.lozinka));
                command.Parameters.Add(new SqlParameter("@KorisnickoIme", this.korisnickoIme));
                command.Parameters.Add(new SqlParameter("@Pol", this.pol.ToString()));
                command.Parameters.Add(new SqlParameter("@Adresa", this.adresa));
                command.Parameters.Add(new SqlParameter("@Email", this.email));
                command.Parameters.Add(new SqlParameter("@TipKorisnika", this.tipKorisnika.ToString()));
                command.Parameters.Add(new SqlParameter("@Active", this.active));

                command.ExecuteNonQuery();
            }
        }
    }
}

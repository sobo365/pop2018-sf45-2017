﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvioKarte.Model
{
    public class Karta : INotifyPropertyChanged, ICloneable
    {
        public int Id { get; set; }

        private Let let;
        public Let Let {
            get { return let; }
            set
            {
                let = value;
                OnPropertyChanged("Let");
            }
        }

        private Korisnik putnik;
        public Korisnik Putnik {
            get { return putnik; }
            set
            {
                putnik = value;
                OnPropertyChanged("Puntik");
            }
        }

        private Sediste sediste;
        public Sediste Sediste
        {
            get { return sediste; }
            set
            {
                sediste = value;
                OnPropertyChanged("Sediste");
            }
        }
        private bool active;
        public bool Active {
            get { return active; }
            set
            {
                active = value;
                OnPropertyChanged("Active");
            }
        }
        private string ime;

        public string Ime
        {
            get { return ime; }
            set
            {
                ime = value;
                OnPropertyChanged("Ime");
            }
        }

        private string prezime;

        public string Prezime
        {
            get { return prezime; }
            set
            {
                OnPropertyChanged("Prezime");
                prezime = value;
            }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set
            {
                email = value;
                OnPropertyChanged("Email");
            }
        }

        private string adresa;

        public string Adresa
        {
            get { return adresa; }
            set
            {
                adresa = value;
                OnPropertyChanged("Adresa");
            }
        }

        private EPol pol;

        public EPol Pol
        {
            get { return pol; }
            set
            {
                pol = value;
                OnPropertyChanged("Pol");
            }
        }

        private double cena;
        public double Cena
        {
            get { return cena; }
            set
            {
                cena = value;
                OnPropertyChanged("Cena");
            }
        }

        private EKlasaSedista klasaSedista;

        public EKlasaSedista KlasaSedista
        {
            get { return klasaSedista; }
            set {
                
                klasaSedista = value;
                OnPropertyChanged("KlasaSedista");
            }
        }


        public Karta(Let letInput, Sediste sedisteInput, Korisnik putnikInput)
        {
            Let = letInput;
            Sediste = sedisteInput;
            Putnik = putnikInput;
            Active = true;
            
        }

        public void populateEntities()
        {
            if (putnik != null)
            {
                this.ime = putnik.Ime;
                prezime = putnik.Prezime;
                pol = putnik.Pol;
                adresa = putnik.Adresa;
                email = putnik.Email;
            }
        }

        public Karta()
        {
            Active = true;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return Id.ToString() + " - " + let.ToString();
        }

        public object Clone()
        {
            Karta karta = new Karta
            {
                Id = Id,
                Let = Let,
                Sediste = Sediste,
                Putnik = Putnik,
                Ime = Ime,
                Prezime = Prezime,
                Adresa = Adresa,
                Pol = Pol,
                Email = Email,
                Cena = Cena,
                KlasaSedista = KlasaSedista,
                Active = Active
            };
        return karta;
        }

        public void Insert()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Karte(Sediste, KlasaSedista,PutnikID, LetID, Ime, Prezime, Pol, Adresa, Email,Cena, Active)
                                        values(@Sediste, @KlasaSedista, @PutnikID, @LetID, @Ime, @Prezime, @Pol, @Adresa, @Email,@Cena, @Active)";

                command.Parameters.Add(new SqlParameter("@Sediste", this.sediste.ToString()));
                command.Parameters.Add(new SqlParameter("@PutnikID", this.putnik.Id));
                command.Parameters.Add(new SqlParameter("@LetID", this.let.Id));
                command.Parameters.Add(new SqlParameter("@Ime", this.ime));
                command.Parameters.Add(new SqlParameter("@KlasaSedista", this.klasaSedista.ToString()));
                command.Parameters.Add(new SqlParameter("@Prezime", this.prezime));
                command.Parameters.Add(new SqlParameter("@Pol", this.pol.ToString()));
                command.Parameters.Add(new SqlParameter("@Adresa", this.adresa));
                command.Parameters.Add(new SqlParameter("@Email", this.email));
                command.Parameters.Add(new SqlParameter("@Cena", this.cena));
                command.Parameters.Add(new SqlParameter("@Active", true));

                command.ExecuteNonQuery();
            }
        }

        public void Delete()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update karte set Active = 0 where Id = @id";

                command.Parameters.Add(new SqlParameter("@id", this.Id));

                command.ExecuteNonQuery();
            }
        }

    }

    

}

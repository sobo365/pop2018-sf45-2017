﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvioKarte.Model
{
    public class Sediste:INotifyPropertyChanged, ICloneable
    {
        
        public char Kolona { get; set; }
        public int Red { get; set; }
        private EKlasaSedista klasaSedista;
        public EKlasaSedista KlasaSedista
        {
            get { return klasaSedista; }
            set
            {
                klasaSedista = value;
                OnPropertyChanged("KlasaSedista");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public bool Zauzeto { get; set; }

        public Sediste(int redInput, char kolonaInput, EKlasaSedista klasaSedistaInput)
        {
            Red = redInput;
            Kolona = kolonaInput;
            KlasaSedista = klasaSedistaInput;
            Zauzeto = false;
        }
        public Sediste()
        {

        }

        public override string ToString()
        {
            try
            {
                return Red.ToString() + "" + Kolona.ToString();
            }
            catch(Exception ex)
            {
                return "null";
            }
        }

        public object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
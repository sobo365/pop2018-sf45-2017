﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AvioKarte.Model
{
   public class Aviokompanija : INotifyPropertyChanged, ICloneable
    {
        public int Id { get; set; }
        private string sifra;
        public string Sifra
        {
            get { return sifra; }
            set
            {
                sifra = value;
                OnPropertyChanged("Sifra");
            }
        }
        private string naziv;
        public string Naziv {
            get { return naziv; }
            set
            {
                naziv = value;
                OnPropertyChanged("Naziv");
            }
        }
        private bool active;
        public bool Active {
            get { return active; }
            set
            {
                active = value;
                OnPropertyChanged("Active");
            }
        }

        public Aviokompanija(string sifra, string naziv)
        {
            Sifra = sifra;
            Naziv = naziv;
            Active = true;
        }
        public Aviokompanija()
        {
            Active = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return naziv;
        }

        public object Clone()
        {
            Aviokompanija aviokompanija = new Aviokompanija()
            {
                Id = this.Id,
                Sifra = this.Sifra,
                Naziv = this.Naziv,
                Active = this.Active
            };
            return aviokompanija;
        }

        public void Insert()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Aviokompanije(Sifra, Naziv, Active)
                                        values(@Sifra, @Naziv, @Active)";

                command.Parameters.Add(new SqlParameter("@Sifra", this.sifra));
                command.Parameters.Add(new SqlParameter("@Naziv", this.naziv));
                command.Parameters.Add(new SqlParameter("@Active", this.active));

                command.ExecuteNonQuery();
            }
        }

        public void Delete()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update aviokompanije set Active = 0 where Id = @id";

                command.Parameters.Add(new SqlParameter("@id", this.Id));

                command.ExecuteNonQuery();
            }
        }

        public void Update()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update aviokompanije set Sifra = @Sifra, Naziv =  @Naziv, Active = @Active where Id = @id";

                command.Parameters.Add(new SqlParameter("@id", this.Id));
                command.Parameters.Add(new SqlParameter("@Sifra", this.sifra));
                command.Parameters.Add(new SqlParameter("@Naziv", this.naziv));
                command.Parameters.Add(new SqlParameter("@Active", this.active));

                command.ExecuteNonQuery();
            }
        }
    }
}

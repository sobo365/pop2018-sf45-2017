﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvioKarte.Model
{
    public class Avion: INotifyPropertyChanged, ICloneable
    {
        public int Id { get; set; }
        private string sifraAviona;
        public string SifraAviona {
            get { return sifraAviona; }
            set
            {
                sifraAviona = value;
                OnPropertyChanged("SifraAviona");
            }
        }
        private string nazivAviona;
        public string NazivAviona {
            get { return nazivAviona; }
            set
            {
                nazivAviona = value;
                OnPropertyChanged("NazivAviona");
            }
        }
        
        private Aviokompanija aviokompanija;
        public Aviokompanija Aviokompanija {
            get { return aviokompanija; }
            set
            {
                aviokompanija = value;
                OnPropertyChanged("Aviokompanija");
            }
        }
        private bool poletio;
        public bool Poletio {
            get { return poletio; }
            set
            {
                poletio = value;
                OnPropertyChanged("Poletio");
            }
        }
        
        private int brojRedovaSedistaB;
        public int BrojRedovaSedistaB
        {
            get { return brojRedovaSedistaB; }
            set
            {
                brojRedovaSedistaB = value;
                OnPropertyChanged("BrojRedovaSedistaB");
            }
        }

        private int brojKolonaSedistaB;
        public int BrojKolonaSedistaB
        {
            get { return brojKolonaSedistaB; }
            set
            {
                brojKolonaSedistaB = value;
                OnPropertyChanged("BrojKolonaSedistaB");
            }
        }

        private int brojRedovaSedistaE;
        public int BrojRedovaSedistaE
        {
            get { return brojRedovaSedistaE; }
            set
            {
                brojRedovaSedistaE = value;
                OnPropertyChanged("BrojRedovaSedistaE");
            }
        }
        
        private int brojKolonaSedistaE;
        public int BrojKolonaSedistaE
        {
            get { return brojKolonaSedistaE; }
            set
            {
                brojKolonaSedistaE = value;
                OnPropertyChanged("BrojKolonaSedistaE");
            }
        }
        private bool active;
        public bool Active {
            get { return active; }
            set
            {
                active = value;
                OnPropertyChanged("Active");
            }
        }
        public List<Sediste> ListaSedistaBiznis { get; set; }
        public List<Sediste> ListaSedistaEkonomska { get; set; }

        public Avion(string sifraAvionaInput, string nazivAvionaInput, Aviokompanija aviokompanijaInput,int brojRedovaSedistaBInput,int brojKolonaSedistaBInput,int brojRedovaSedistaEInput,int brojKolonaSedistaEInput   )
        {
            ListaSedistaBiznis = new List<Sediste>();
            ListaSedistaEkonomska = new List<Sediste>();
            SifraAviona = sifraAvionaInput;
            NazivAviona = nazivAvionaInput;
            Aviokompanija = aviokompanijaInput;
            BrojKolonaSedistaB = brojKolonaSedistaBInput;
            BrojRedovaSedistaB = brojRedovaSedistaBInput;
            BrojKolonaSedistaE = brojKolonaSedistaEInput;
            brojRedovaSedistaE = brojRedovaSedistaEInput;
            Active = true;
            generateSeats();
        }

        public Avion()
        {
            ListaSedistaBiznis = new List<Sediste>();
            ListaSedistaEkonomska = new List<Sediste>();
            Active = true;
            Poletio = false;
            generateSeats();
        }

        public void generateSeats()
        {
            char[] alphabet = "abcdefghijklmnopqrstuvwxyz".ToUpper().ToCharArray();

            ListaSedistaEkonomska = new List<Sediste>();
            ListaSedistaBiznis = new List<Sediste>();
            for (int i = 1; i <= BrojRedovaSedistaE; i++)
            {
                for(int j = 0; j < BrojKolonaSedistaE; j++)
                {
                    ListaSedistaEkonomska.Add(new Sediste(i, alphabet[j], EKlasaSedista.EKONOMSKA));
                }
                
            }
            for(int i = 1; i <= BrojKolonaSedistaB; i++)
            {
                for(int j = 0; j < BrojRedovaSedistaB; j++)
                { 
                    ListaSedistaBiznis.Add(new Sediste(i, alphabet[j], EKlasaSedista.BIZNIS));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        

        public override string ToString()
        {
            return NazivAviona + "-" + SifraAviona;
        }

        public object Clone()
        {
            Avion avion = new Avion()
            {
                Id = Id,
                SifraAviona = SifraAviona,
                NazivAviona = NazivAviona,
                Aviokompanija = Aviokompanija,
                BrojKolonaSedistaB = BrojKolonaSedistaB,
                BrojRedovaSedistaB = BrojRedovaSedistaB,
                BrojRedovaSedistaE = BrojRedovaSedistaE,
                BrojKolonaSedistaE = BrojKolonaSedistaE,
                ListaSedistaBiznis = ListaSedistaBiznis,
                ListaSedistaEkonomska = ListaSedistaEkonomska,
                Poletio = Poletio,
                Active = Active
            };
            return avion;
        }

        public void Insert()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Avioni(SifraAviona, NazivAviona,IdAviokompanije, BrojRedovaSedistaB, BrojKolonaSedistaB, BrojRedovaSedistaE, BrojKolonaSedistaE, Active)
                                                    values(@SifraAviona, @NazivAviona,@IdAviokompanije, @BrojRedovaSedistaB, @BrojKolonaSedistaB, @BrojRedovaSedistaE, @BrojKolonaSedistaE, @Active)";

                command.Parameters.Add(new SqlParameter("@SifraAviona", this.sifraAviona));
                command.Parameters.Add(new SqlParameter("@NazivAviona", this.nazivAviona));
                command.Parameters.Add(new SqlParameter("@IdAviokompanije", this.aviokompanija.Id));
                command.Parameters.Add(new SqlParameter("@BrojRedovaSedistaB", this.BrojRedovaSedistaB));
                command.Parameters.Add(new SqlParameter("@BrojKolonaSedistaB", this.BrojKolonaSedistaB));
                command.Parameters.Add(new SqlParameter("@BrojRedovaSedistaE", this.BrojRedovaSedistaE));
                command.Parameters.Add(new SqlParameter("@BrojKolonaSedistaE", this.BrojKolonaSedistaE));
                command.Parameters.Add(new SqlParameter("@Active", true));

                command.ExecuteNonQuery();
            }
        }

        public void Delete()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Avioni set Active = 0 where Id = @id";

                command.Parameters.Add(new SqlParameter("@id", this.Id));

                command.ExecuteNonQuery();
            }
        }

        public void Update()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Avioni set SifraAviona = @SifraAviona, NazivAviona = @NazivAviona, IdAviokompanije = @IdAviokompanije,
                                      BrojRedovaSedistaB = @BrojRedovaSedistaB, BrojKolonaSedistaB = @BrojKolonaSedistaB ,
                                      BrojRedovaSedistaE = @BrojRedovaSedistaE, BrojKolonaSedistaE = @BrojKolonaSedistaE , Active = @Active where Id = @id"
                                        ;

                command.Parameters.Add(new SqlParameter("@id", this.Id));
                command.Parameters.Add(new SqlParameter("@SifraAviona", this.sifraAviona));
                command.Parameters.Add(new SqlParameter("@NazivAviona", this.nazivAviona));
                command.Parameters.Add(new SqlParameter("@IdAviokompanije", this.aviokompanija.Id));
                command.Parameters.Add(new SqlParameter("@BrojRedovaSedistaB", this.BrojRedovaSedistaB));
                command.Parameters.Add(new SqlParameter("@BrojKolonaSedistaB", this.BrojKolonaSedistaB));
                command.Parameters.Add(new SqlParameter("@BrojRedovaSedistaE", this.BrojRedovaSedistaE));
                command.Parameters.Add(new SqlParameter("@BrojKolonaSedistaE", this.BrojKolonaSedistaE));
                command.Parameters.Add(new SqlParameter("@Active", true));

                command.ExecuteNonQuery();
            }
        }
    }
}

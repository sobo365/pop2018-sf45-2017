﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvioKarte.Model
{
    public class Aerodrom : INotifyPropertyChanged, ICloneable
    {
        public int id { get; set; }
        
        private string sifra;
        public string Sifra
        {
            get { return sifra; }
            set
            {
                sifra = value;
                OnPropertyChanged("Sifra");
            }
        }
        private string naziv;
        public string Naziv
        {
            get { return naziv; }
            set
            {
                naziv = value;
                OnPropertyChanged("Naziv");
            }

        }
        private string grad;
        public string Grad
        {
            get { return grad; }
            set
            {
                grad = value;
                OnPropertyChanged("Grad");
            }

        }
        private int brojKapija;
        public int BrojKapija
        {
            get { return brojKapija; }
            set
            {
                brojKapija = value;
                OnPropertyChanged("BrojKapija");
            }
        }
        private List<int> listaKapija;
        public List<int> ListaKapija
        {
            get { return listaKapija; }
            set
            {
                listaKapija = value;
                OnPropertyChanged("ListaKapija");
            }
        }
        private bool active;
        public bool Active {
            get { return active; }
            set
            {
                active = value;
                OnPropertyChanged("Active");
            }
        }

        public void generateGates()
        {
            listaKapija = new List<int>();
            for (int i = 1; i <= brojKapija; i++)
            {
                listaKapija.Add(i);
            }
        }

        public Aerodrom()
        {
            Active = true;
            generateGates();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public object Clone()
        {
            Aerodrom aerodrom = new Aerodrom()
            {
                id = this.id,
                Sifra = this.Sifra,
                Grad = this.Grad,
                Naziv = this.Naziv,
                ListaKapija = ListaKapija,
                BrojKapija = BrojKapija,
                Active = this.Active
            };
            return aerodrom;
        }

        public override string ToString()
        {
            return $"{Naziv}";
        }

        public void Insert()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Aerodromi(Sifra, Grad, Naziv, BrojKapija, Active)
                                        values(@Sifra, @Grad, @Naziv, @BrojKapija, @Active)";
                
                command.Parameters.Add(new SqlParameter("@Sifra", this.sifra));
                command.Parameters.Add(new SqlParameter("@Grad", this.grad));
                command.Parameters.Add(new SqlParameter("@Naziv", this.naziv));
                command.Parameters.Add(new SqlParameter("@BrojKapija", this.brojKapija));
                command.Parameters.Add(new SqlParameter("@Active", this.active));

                command.ExecuteNonQuery();
            }
        }

        public void Delete()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Aerodromi set Active = 0 where Id = @id";

                command.Parameters.Add(new SqlParameter("@id", this.id));

                command.ExecuteNonQuery();
            }
        }

        public void Update()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update Aerodromi set Sifra = @Sifra, Grad= @Grad, Naziv = @Naziv, BrojKapija = @BrojKapija, Active = @Active where id = @id";

                command.Parameters.Add(new SqlParameter("@id", this.id));
                command.Parameters.Add(new SqlParameter("@Sifra", this.sifra));
                command.Parameters.Add(new SqlParameter("@Grad", this.grad));
                command.Parameters.Add(new SqlParameter("@Naziv", this.naziv));
                command.Parameters.Add(new SqlParameter("@BrojKapija", this.brojKapija));
                command.Parameters.Add(new SqlParameter("@Active", this.active));

                command.ExecuteNonQuery();
            }
        }
    }
}

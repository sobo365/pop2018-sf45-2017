﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AvioKarte.Model;
using AvioKarte.Utility;
using AvioKarte.Database;

namespace AvioKarte
{
    /// <summary>
    /// Interaction logic for EditAvioniWindow.xaml
    /// </summary>
    public partial class EditAvioniWindow : Window
    {
        public enum Opcija { DODAVANJE, IZMENA };
        private Opcija opcija;
        private Avion avion;
        public EditAvioniWindow(Avion avion, Opcija opcija)
        {
            InitializeComponent();
            this.avion = avion;
            this.opcija = opcija;
            DataContext = avion;
            cbAviokompanija.ItemsSource = Data.Instance.Aviokompanije.Where(a => a.Active);
            cbAviokompanija.SelectedIndex = 1;


            if (opcija.Equals(Opcija.IZMENA))
            {
                txtSifraAviona.IsEnabled = false;
                txtBrojKolonaSedistaB.IsEnabled = false;
                txtBrojKolonaSedistaE.IsEnabled = false;
                txtBrojRedovaSedistaB.IsEnabled = false;
                txtBrojRedovaSedistaE.IsEnabled = false;
            }
        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if(txtNazivAviona.Text.Equals(string.Empty) || txtSifraAviona.Text.Equals(string.Empty) || txtBrojRedovaSedistaE.Text.Equals(string.Empty) || txtBrojRedovaSedistaE.Text.Equals(string.Empty) || txtBrojKolonaSedistaE.Text.Equals(string.Empty) || txtBrojKolonaSedistaB.Text.Equals(string.Empty))
            {
                lblErrorMessage.Content = "Niste uneli sve podatke!";
            }
            else if(avion.BrojKolonaSedistaE > avion.BrojRedovaSedistaE )
            {
                lblErrorMessage.Content = "Broj kolona ne može da bude veći od broja redova";
            }
            else
            {
                if (opcija.Equals(Opcija.DODAVANJE))
                {
                    avion.Insert();
                    Data.Instance.UcitajAvioneDB();
                    MessageBox.Show($"Uspesno ste dodali novi avion: {avion}");
                    this.Close();
                }
                if (opcija.Equals(Opcija.IZMENA))
                {

                    avion.Update();
                    Data.Instance.UcitajAvioneDB();
                    this.Close();

                }
            }
            
            
        }

        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

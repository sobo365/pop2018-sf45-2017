﻿using AvioKarte.Model;
using AvioKarte.Pages;
using AvioKarte.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AvioKarte
{
    /// <summary>
    /// Interaction logic for UnosPodataka.xaml
    /// </summary>
    public partial class UnosPodataka : Window
    {
        Let let;
        bool povratni;
        Karta karta;
        public UnosPodataka(Let let, bool povratni, Karta karta)
        {
            this.karta = karta;
            this.let = let;
            this.povratni = povratni;
            InitializeComponent();
            cbPol.ItemsSource = Enum.GetValues(typeof(EPol)).Cast<EPol>();
            DataContext = karta;
        }

        private void btnPotvrdi_Click(object sender, RoutedEventArgs e)
        {
            if(txtIme.Text.Equals(string.Empty) || txtPrezime.Text.Equals(string.Empty) || txtEmail.Text.Equals(string.Empty) || txtAdresa.Text.Equals(string.Empty))
            {
                lblErrorMessage.Content = "Niste uneli sve podatke!";
            }
            else
            {
                if (!povratni)
                {
                    karta.Putnik = new Korisnik { Id = 0 };
                    karta.Insert();
                    KartaDetaljnoWindow kdw = new KartaDetaljnoWindow(karta);
                    kdw.Show();
                    this.Close();
                }
                if (povratni)
                {
                    karta.Putnik.Id = 0;
                    Tool.Instance.kartaDoOdredista = karta;
                    KartaDetaljnoWindow kdw = new KartaDetaljnoWindow(karta);
                    kdw.Show();
                    this.Close();
                }
                Database.Data.Instance.UcitajKarteDB();
            }
            
        }
    }
}

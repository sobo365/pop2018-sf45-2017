﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AvioKarte.Database;
using AvioKarte.Model;
using AvioKarte.Utility;

namespace AvioKarte
{
    /// <summary>
    /// Interaction logic for KarteWindow.xaml
    /// </summary>
    public partial class KarteWindow : Window
    {
        ICollectionView view;
        public KarteWindow()
        {
           
            InitializeComponent();        
            view = CollectionViewSource.GetDefaultView(Data.Instance.Karte);
            view.Filter = Filter;
            listKarte.ItemsSource = Data.Instance.Karte;
            listKarte.IsSynchronizedWithCurrentItem = true;

            DatumOd.SelectedDate = DateTime.Now.AddDays(1);
            DatumDo.SelectedDate = DateTime.Now.AddDays(200);
        }

        private bool Filter(object obj)
        {
            Karta karta = obj as Karta;
            return karta.Active;
        }

        private void btnIzlaz_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDodajKartu_Click(object sender, RoutedEventArgs e)
        {
            EditKarteWindow ekw = new EditKarteWindow(new Karta(), EditKarteWindow.Opcija.DODAVANJE);
            ekw.Show();
        }

        private void btnIzmeniKartu_Click(object sender, RoutedEventArgs e)
        {
            Karta karta = listKarte.SelectedItem as Karta;
            if(karta != null)
            {
                Karta clone = karta.Clone() as Karta;
                EditKarteWindow ekw = new EditKarteWindow(karta, EditKarteWindow.Opcija.IZMENA);
                ekw.Show();
                
            }
        }

        private void btnObrisiKartu_Click(object sender, RoutedEventArgs e)
        {
            Karta selektovanaKarta = listKarte.SelectedItem as Karta;
            if (selektovanaKarta != null)
            {
                if (MessageBox.Show($"Da li zelite da obrisete kartu: {selektovanaKarta}", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    selektovanaKarta.Active = false;
                }
            }
            else
            {
                MessageBox.Show("Selektujte kartu!");
            }
            view.Refresh();
        }

        private void btnOpadajuce_Click(object sender, RoutedEventArgs e)
        {
            IEnumerable<Karta> query = from k in Data.Instance.Karte
                                     orderby k.Let.CenaKarte descending
                                     where k.Active == true
                                     select k;

            listKarte.ItemsSource = query;
            view.Refresh();
        }

        private void btnRastuce_Click(object sender, RoutedEventArgs e)
        {
            IEnumerable<Karta> query = from k in Data.Instance.Karte
                                       orderby k.Let.CenaKarte
                                       where k.Active == true
                                       select k;

            listKarte.ItemsSource = query;
            view.Refresh();
        }

        private void btnPretrazi_Click(object sender, RoutedEventArgs e)
        {
            if(rbBiznis.IsChecked == true)
            {
                listKarte.ItemsSource = Data.Instance.Karte.Where(k => k.Let.BrojLeta.ToLower().Contains(txtPretragaBrojLeta.Text.ToLower()) && k.Ime.ToLower().Contains(txtNazivPutnika.Text.ToLower()) &&
                                                               k.Let.VremePolaska.Date >= DatumOd.SelectedDate.Value && k.Let.VremeDolaska <= DatumDo.SelectedDate.Value && k.KlasaSedista.Equals(EKlasaSedista.BIZNIS) &&
                                                               k.Let.Polaziste.Naziv.ToLower().Contains(txtAerodromOd.Text.ToLower()) && k.Let.Odrediste.Naziv.ToLower().Contains(txtAerodromDo.Text.ToLower()));
            }
            if (rbEkonomska.IsChecked == true)
            {
                listKarte.ItemsSource = Data.Instance.Karte.Where(k => k.Let.BrojLeta.ToLower().Contains(txtPretragaBrojLeta.Text.ToLower()) && k.Ime.ToLower().Contains(txtNazivPutnika.Text.ToLower()) &&
                                                               k.Let.VremePolaska.Date >= DatumOd.SelectedDate.Value && k.Let.VremeDolaska <= DatumDo.SelectedDate.Value && k.KlasaSedista.Equals(EKlasaSedista.EKONOMSKA) &&
                                                               k.Let.Polaziste.Naziv.ToLower().Contains(txtAerodromOd.Text.ToLower()) && k.Let.Odrediste.Naziv.ToLower().Contains(txtAerodromDo.Text.ToLower()));
            }
            if (rbObe.IsChecked == true)
            {
                listKarte.ItemsSource = Data.Instance.Karte.Where(k => k.Let.BrojLeta.ToLower().Contains(txtPretragaBrojLeta.Text.ToLower()) && k.Ime.ToLower().Contains(txtNazivPutnika.Text.ToLower()) &&
                                                                               k.Let.VremePolaska.Date >= DatumOd.SelectedDate.Value && k.Let.VremeDolaska <= DatumDo.SelectedDate.Value &&
                                                                               k.Let.Polaziste.Naziv.ToLower().Contains(txtAerodromOd.Text.ToLower()) && k.Let.Odrediste.Naziv.ToLower().Contains(txtAerodromDo.Text.ToLower()));
            }
            
        }

        private void btnPonistiPretragu_Click(object sender, RoutedEventArgs e)
        {
            txtPretragaBrojLeta.Text = string.Empty;
            txtNazivPutnika.Text = string.Empty;
            txtAerodromDo.Text = string.Empty;
            txtAerodromOd.Text = string.Empty;
            DatumOd.SelectedDate = DateTime.Now.AddDays(1);
            DatumDo.SelectedDate = DateTime.Now.AddDays(200);

            IEnumerable<Karta> query = from k in Data.Instance.Karte
                                       orderby k.Let.CenaKarte
                                       where k.Active == true
                                       select k;

            listKarte.ItemsSource = query;
            view.Refresh();
        }

        private void btnKarta_Click(object sender, RoutedEventArgs e)
        {
            Karta karta = listKarte.SelectedItem as Karta;
            if (karta != null)
            {
                KartaDetaljnoWindow kdw = new KartaDetaljnoWindow(karta);
                kdw.Show();
            }
        }
    }
}

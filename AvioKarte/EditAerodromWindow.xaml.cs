﻿using AvioKarte.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AvioKarte.Database;
using AvioKarte.Utility;

namespace AvioKarte
{
    /// <summary>
    /// Interaction logic for EditAerodromWindow.xaml
    /// </summary>
    public partial class EditAerodromWindow : Window
    {
        public enum Opcija { DODAVANJE, IZMENA };
        private Opcija opcija;
        private Aerodrom aerodrom;
        public EditAerodromWindow(Aerodrom aerodrom, Opcija opcija)
        {
            InitializeComponent();
            this.aerodrom = aerodrom;
            this.opcija = opcija;
            this.DataContext = aerodrom;            

            if (opcija.Equals(Opcija.IZMENA))
            {
                txtSifra.IsEnabled = false;
            }
        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {

            if (txtGrad.Text.Equals(string.Empty) || txtNaziv.Text.Equals(string.Empty) || txtSifra.Text.Equals(string.Empty))
            {
                lblErrorMessage.Content = "Niste uneli sve podatke!";
            }
            else if(aerodrom.BrojKapija < 0)
            {
                lblErrorMessage.Content = "Broj kapija mora biti veci od nula!";
            }
            else
            {
                if (opcija.Equals(Opcija.IZMENA))
                {
                    aerodrom.Update();
                    Data.Instance.UcitajAerodromeDB();
                    this.Close();
                }
                if (opcija.Equals(Opcija.DODAVANJE))
                {

                    if (!Tool.Instance.proveriPostojeceSifreAerodroma(aerodrom.Sifra.Trim()))
                    {
                        aerodrom.Insert();
                        Data.Instance.UcitajAerodromeDB();
                        MessageBox.Show($"Usepsno ste dodali novi aerodrom: {aerodrom}");
                        this.Close();
                    }
                    else
                    {
                        lblErrorMessage.Content = "Ova sifre se vec koristi";
                    }
                }
            }


            
        }

        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

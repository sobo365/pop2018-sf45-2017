﻿using AvioKarte.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AvioKarte
{
    /// <summary>
    /// Interaction logic for KartaDetaljnoWindow.xaml
    /// </summary>
    public partial class KartaDetaljnoWindow : Window
    {
        public KartaDetaljnoWindow(Karta karta)
        {
            DataContext = karta;
            InitializeComponent();
            if(karta.Sediste.KlasaSedista.Equals(EKlasaSedista.EKONOMSKA))
            {
                lbllKlasa.Content = "ECONOMY";
            }
            else
            {
                lbllKlasa.Content = "BUSINESS";
            }

            if(karta.Pol.Equals(EPol.M))
            {
                lblPol.Content = "MR. ";
            }
            else
            {
                lblPol.Content = "MRS. ";
            }
        }
    }
}

﻿using AvioKarte.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace AvioKarte.Database
{
    class Data
    {
        public ObservableCollection<Korisnik> Korisnici { get; set; }
        public ObservableCollection<Aerodrom> Aerodromi { get; set; }
        public ObservableCollection<Let> Letovi { get; set; }
        public ObservableCollection<Aviokompanija> Aviokompanije { get; set; }
        public ObservableCollection<Avion> Avioni { get; set; }
        public ObservableCollection<Karta> Karte { get; set; }
        public String UlogovanKorisnik { get; set; }
        public const string CONNECTION_STRING = @"Data Source=DESKTOP-ILJ5VCI;Initial Catalog=AvioKarte;Integrated Security=True;";


        private Data()
        {
            Aviokompanije = new ObservableCollection<Aviokompanija>();
            Korisnici = new ObservableCollection<Korisnik>();
            Aerodromi = new ObservableCollection<Aerodrom>();
            Letovi = new ObservableCollection<Let>();
            Avioni = new ObservableCollection<Avion>();
            Karte = new ObservableCollection<Karta>();

            UcitajAerodromeDB();
            UcitajAviokompanijeDB();
            UcitajAvioneDB();
            UcitajKorisnikeDB();
            UcitajLetoveDB();
            UcitajKarteDB();

        }



        private static Data _instance = null;

        public static Data Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Data();
                }
                return _instance;
            }
        }

        public void LoadData()
        {
            UcitajAerodromeDB();
            UcitajAviokompanijeDB();
            UcitajAvioneDB();
            UcitajKorisnikeDB();
            UcitajLetoveDB();
            UcitajKarteDB();
        }

       

        public void UcitajLetoveDB()
        {
            Letovi.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Letovi";

                SqlDataAdapter daLetovi = new SqlDataAdapter();
                daLetovi.SelectCommand = command;

                DataSet dsLetovi = new DataSet();
                daLetovi.Fill(dsLetovi, "Letovi");

                foreach (DataRow row in dsLetovi.Tables["Letovi"].Rows)
                {
                    Let let = new Let();
                    let.Id = (int)row["Id"];
                    let.BrojLeta = (string)row["BrojLeta"];
                    let.VremePolaska = (DateTime)row["VremePolaska"];
                    let.VremeDolaska = (DateTime)row["VremeDolaska"];
                    let.CenaKarte = (double)row["CenaKarte"];
                    let.Pilot = (string)row["Pilot"];
                    let.Active = (bool)row["Active"];
                    int PolazniAerodromID = (int)row["PolazniAerodromID"];
                    int DolazniAerodromID = (int)row["DolazniAerodromID"];
                    int AviokompanijaID = (int)row["AviokompanijaID"];
                    int AvionID = (int)row["AvionID"];
                    foreach(Aviokompanija ak in Aviokompanije)
                    {
                        if(ak.Id == AviokompanijaID)
                        {
                            let.Aviokompanija = ak;
                            break;
                        }
                    }
                    foreach(Aerodrom ar in Aerodromi)
                    {
                        if(ar.id == PolazniAerodromID)
                        {
                            let.Polaziste = ar;
                            break;
                        }
                    }
                    foreach (Aerodrom ar in Aerodromi)
                    {
                        if (ar.id == DolazniAerodromID)
                        {
                            let.Odrediste = ar;
                            break;
                        }
                    }
                    foreach(Avion av in Avioni)
                    {
                        if(av.Id == AvionID)
                        {
                            let.Avion = av;
                            break;
                        }
                    }
                    Letovi.Add(let);
                    

                }
            }
        }

        public void UcitajKorisnikeDB()
        {
            Korisnici.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Korisnici";

                SqlDataAdapter daKorisnici = new SqlDataAdapter();
                daKorisnici.SelectCommand = command;

                DataSet dsKorisnici = new DataSet();
                daKorisnici.Fill(dsKorisnici, "Korisnici");

                foreach (DataRow row in dsKorisnici.Tables["Korisnici"].Rows)
                {
                    Korisnik korisnik = new Korisnik();
                    korisnik.Id = (int)row["Id"];
                    korisnik.Ime = (string)row["Ime"];
                    korisnik.Prezime = (string)row["Prezime"];
                    korisnik.KorisnickoIme = (string)row["KorisnickoIme"];
                    korisnik.Lozinka = (string)row["Lozinka"];
                    korisnik.Adresa = (string)row["Adresa"];
                    korisnik.Email = (string)row["Email"];
                    korisnik.Pol = (EPol)Enum.Parse(typeof(EPol), (string)row["Pol"]);
                    korisnik.TipKorisnika = (ETipKorisnika)Enum.Parse(typeof(ETipKorisnika), (string)row["TipKorisnika"]);
                    korisnik.Active = (bool)row["Active"];           
                    Korisnici.Add(korisnik);
                    
                    
                    
                }
            }
        }

        public void UcitajAvioneDB()
        {
            Avioni.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from Avioni";

                SqlDataAdapter daAvioni = new SqlDataAdapter();
                daAvioni.SelectCommand = command;

                DataSet dsAvioni = new DataSet();
                daAvioni.Fill(dsAvioni, "Avioni");

                foreach(DataRow row in dsAvioni.Tables["Avioni"].Rows)
                {
                    Avion avion = new Avion();
                    avion.Id = (int)row["Id"];
                    avion.SifraAviona = (string)row["SifraAviona"];
                    avion.NazivAviona = (string)row["NazivAviona"];
                    int IdAviokompanije = (int)row["IdAviokompanije"];
                    avion.BrojRedovaSedistaB = (int)row["BrojRedovaSedistaB"];
                    avion.BrojKolonaSedistaB = (int)row["BrojKolonaSedistaB"];
                    avion.BrojRedovaSedistaE = (int)row["BrojRedovaSedistaE"];
                    avion.BrojKolonaSedistaE = (int)row["BrojKolonaSedistaE"];
                    avion.Active = (bool)row["Active"];
                    foreach(Aviokompanija ak in Aviokompanije)
                    {
                        if(ak.Id == IdAviokompanije)
                        {
                            avion.Aviokompanija = ak;
                            break;
                        }
                    }
                    avion.generateSeats();                   
                    Avioni.Add(avion);
                    
                    
                }
            }
        }

        public void UcitajAviokompanijeDB()
        {
            Aviokompanije.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM Aviokompanije";

                SqlDataAdapter daAviokompanije = new SqlDataAdapter();
                daAviokompanije.SelectCommand = command;

                DataSet dsAviokompanije = new DataSet();
                daAviokompanije.Fill(dsAviokompanije, "Aviokompanije");

                foreach(DataRow row in dsAviokompanije.Tables["Aviokompanije"].Rows)
                {
                    Aviokompanija aviokompanija = new Aviokompanija();
                    aviokompanija.Id = (int)row["Id"];
                    aviokompanija.Sifra = (string)row["Sifra"];
                    aviokompanija.Naziv = (string)row["Naziv"];
                    aviokompanija.Active = (bool)row["Active"];
                    Aviokompanije.Add(aviokompanija);             
                }
            }
        }

        public void UcitajAerodromeDB()
        {
            Aerodromi.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM Aerodromi";

                SqlDataAdapter daAerodromi = new SqlDataAdapter();
                daAerodromi.SelectCommand = command;

                DataSet dsAerodromi = new DataSet();
                daAerodromi.Fill(dsAerodromi, "Aerodromi");

                foreach (DataRow row in dsAerodromi.Tables["Aerodromi"].Rows)
                {
                    Aerodrom aerodrom = new Aerodrom();
                    aerodrom.id = (int)row["Id"];
                    aerodrom.Sifra = (string)row["Sifra"];
                    aerodrom.Naziv = (string)row["Naziv"];
                    aerodrom.Grad = (string)row["Grad"];
                    aerodrom.BrojKapija = (int)row["BrojKapija"];
                    aerodrom.Active = (bool)row["Active"];  
                    Aerodromi.Add(aerodrom);
                    
                    
                }

            }
        }

        public void UcitajKarteDB()
        {
            Karte.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM Karte";

                SqlDataAdapter daKarte = new SqlDataAdapter();
                daKarte.SelectCommand = command;

                DataSet dsKarte = new DataSet();
                daKarte.Fill(dsKarte, "Karte");

                foreach (DataRow row in dsKarte.Tables["Karte"].Rows)
                {
                    Karta karta = new Karta();
                    karta.Id = (int)row["Id"];
                    karta.Ime = (string)row["Ime"];
                    karta.Prezime = (string)row["Prezime"];
                    karta.Pol = (EPol)Enum.Parse(typeof(EPol), (string)row["Pol"]);
                    karta.Adresa = (string)row["Adresa"];
                    karta.Email = (string)row["Email"];
                    karta.Cena = (double)row["Cena"];
                    karta.KlasaSedista = (EKlasaSedista)Enum.Parse(typeof(EKlasaSedista), (string)row["KlasaSedista"]);
                    int LetID = (int)row["LetID"];
                    int putnikID = (int)row["PutnikID"];
                    string sediste = (string)row["Sediste"];
                    string klasaSedista = (string)row["KlasaSedista"];
                    karta.Active = (bool)row["Active"];


                    karta.Sediste = new Sediste
                    {
                        Kolona = sediste[1],
                        Red = (int) char.GetNumericValue(sediste[0]),
                        KlasaSedista = (EKlasaSedista)Enum.Parse(typeof(EKlasaSedista), klasaSedista, true)
                };
                    


                    foreach (Let let in Letovi)
                    {
                        if (let.Id.Equals(LetID))
                        {
                            karta.Let = let;
                            break;
                        }
                    }

                    foreach (Korisnik k in Korisnici)
                    {
                        if (k.Id.Equals(putnikID))
                        {
                            karta.Putnik = k;
                            break;
                        }
                    }

                    Karte.Add(karta);

                }
            }
        }

}
}
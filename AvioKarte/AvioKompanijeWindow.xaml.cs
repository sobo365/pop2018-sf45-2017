﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AvioKarte.Database;
using AvioKarte.Model;
using AvioKarte.Utility;

namespace AvioKarte
{
    /// <summary>
    /// Interaction logic for AvioKompanijeWindow.xaml
    /// </summary>
    public partial class AvioKompanijeWindow : Window
    {
        ICollectionView view;
        public AvioKompanijeWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.Aviokompanije);
            view.Filter = Filter;
            listAviokompanije.IsSynchronizedWithCurrentItem = true;
            listAviokompanije.ItemsSource = Data.Instance.Aviokompanije;
        }

        private bool Filter(object obj)
        {
            Aviokompanija aviokompanija = obj as Aviokompanija;
            return aviokompanija.Active;
        }

        private void btnDodajAviokompaniju_Click(object sender, RoutedEventArgs e)
        {
            EditAviokompanijeWindow eakw = new EditAviokompanijeWindow(new Aviokompanija(), EditAviokompanijeWindow.Opcija.DODAVANJE);
            eakw.Show();
        }

        private void btnIzmeniAviokompaniju_Click(object sender, RoutedEventArgs e)
        {
            Aviokompanija aviokompanija = listAviokompanije.SelectedItem as Aviokompanija;
            

            if(aviokompanija != null)
            {
                Aviokompanija clone = aviokompanija.Clone() as Aviokompanija;
                EditAviokompanijeWindow edw = new EditAviokompanijeWindow(clone, EditAviokompanijeWindow.Opcija.IZMENA);
                edw.Show();
            }
            else
            {
                MessageBox.Show("Selektujte aviokompaniju!");
            }
            
        }

        private void btnObrisiAviokompaniju_Click(object sender, RoutedEventArgs e)
        {
            Aviokompanija aviokompanija = listAviokompanije.SelectedItem as Aviokompanija;
            if(aviokompanija != null && aviokompanija.Active)
            {
                if (MessageBox.Show($"Da li zelite da obrisete aviokompaniju: {aviokompanija}", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    aviokompanija.Delete();
                    Data.Instance.UcitajAviokompanijeDB();
                }
            }
            else
            {
                MessageBox.Show("Selektujte aviokompaniju!");
            }
            view.Refresh();
        }

        private void btnIzlaz_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

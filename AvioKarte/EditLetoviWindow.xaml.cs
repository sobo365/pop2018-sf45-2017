﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AvioKarte.Database;
using AvioKarte.Model;
using AvioKarte.Utility;

namespace AvioKarte
{
    /// <summary>
    /// Interaction logic for EditLetoviWindow.xaml
    /// </summary>
    public partial class EditLetoviWindow : Window
    {
        public enum Opcija { DODAVANJE, IZMENA};
        private Opcija opcija;
        private Let let;

        public EditLetoviWindow(Let let, Opcija opcija)
        {
            InitializeComponent();
            this.let = let;
            this.opcija = opcija;
            cbAviokompanija.ItemsSource = Data.Instance.Aviokompanije.Where(a => a.Active);
            cbOd.ItemsSource = Data.Instance.Aerodromi.Where(a => a.Active);
            cbDo.ItemsSource = Data.Instance.Aerodromi.Where(a => a.Active);
            DataContext = let;

            if(opcija.Equals(Opcija.DODAVANJE))
            {
                let.VremePolaska = DateTime.Now;
                let.VremeDolaska = DateTime.Now.AddHours(26);
            }

            if (opcija.Equals(Opcija.IZMENA))
            {
                txtBrojLeta.IsEnabled = false;
               
            }
           
            
        }

        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if(txtBrojLeta.Text.Equals(string.Empty) || txtCenaKarte.Text.Equals(string.Empty) || txtPilot.Text.Equals(string.Empty) 
                || cbAviokompanija.SelectedItem == null || cbAvioni.SelectedItem == null || cbOd.SelectedValue == null ||
                cbDo.SelectedValue == null || cbKapija.SelectedValue == null)
            {
                lblErrorMessage.Content = "Niste uneli sve podatke!";
            }
            else if(let.CenaKarte < 0)
            {
                lblErrorMessage.Content = "Cena mora biti pozitivan broj!";
            }
            else
            {
                if (opcija.Equals(Opcija.IZMENA))
                {
                    let.Update();
                    Data.Instance.UcitajLetoveDB();
                    this.Close();
                }
                if (opcija.Equals(Opcija.DODAVANJE))
                {
                    let.Insert();
                    Data.Instance.UcitajLetoveDB();
                    MessageBox.Show($"Uspesno ste dodali novi let: {let}");
                    this.Close();
                }
            }
            
            
        }

        private void cbAviokompanija_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbAvioni.Items.Clear();
            Aviokompanija selektovanaAviokompanija = cbAviokompanija.SelectedItem as Aviokompanija;
            if(selektovanaAviokompanija != null)
            {
                foreach (var Avion in Data.Instance.Avioni)
                {
                    if (selektovanaAviokompanija.Sifra.Equals(Avion.Aviokompanija.Sifra) && Avion.Active)
                    {
                        cbAvioni.Items.Add(Avion);
                    }

                }
            }

        }

        private void cbOd_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbKapija.Items.Clear();
            Aerodrom aerodrom = cbOd.SelectedItem as Aerodrom;
            aerodrom.generateGates();
            foreach(var i in aerodrom.ListaKapija)
            {
                cbKapija.Items.Add(i.ToString());
            }
        }
    }
}

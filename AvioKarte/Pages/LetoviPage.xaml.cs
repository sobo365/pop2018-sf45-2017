﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AvioKarte.Database;
using AvioKarte.Model;

namespace AvioKarte.Pages
{
    /// <summary>
    /// Interaction logic for LetoviPage.xaml
    /// </summary>
    public partial class LetoviPage : Page
    {
        private Frame _mainFrame;
        private ETipKarte tipKarte;
        public LetoviPage(Frame MainFrame)
        {
            _mainFrame = MainFrame;
            InitializeComponent();
            rbEkonomska.IsChecked = true;
            rbPovratna.IsChecked = true;
            populateList();
            dateDatumPovratka.DisplayDateStart = DateTime.Now.AddDays(1);
            dateDatumPovratka.SelectedDate = DateTime.Now.AddDays(7);
            dateDatumPolaska.DisplayDateStart = DateTime.Now;
            dateDatumPolaska.SelectedDate = DateTime.Now;
            
        }

        private void populateList()
        {
            foreach(Aerodrom a in Data.Instance.Aerodromi)
            {
                if(a.Active)
                {
                    lbListaAerodromaFrom.Items.Add(a);
                    lbListaAerodromaTo.Items.Add(a);
                }
                
            }
        }

        private void txtInputFrom_LostFocus(object sender, RoutedEventArgs e)
        {
            lbListaAerodromaFrom.Visibility = Visibility.Hidden;
        }

        private void txtInputFrom_KeyUp(object sender, KeyEventArgs e)
        {
            
            lbListaAerodromaFrom.Items.Clear();
            string aerodromInput = txtInputFrom.Text.Trim();

            foreach (var aerodrom in Data.Instance.Aerodromi)
            {
                if (aerodrom.ToString().ToLower().Contains(aerodromInput.ToLower()) && aerodrom.Active)
                {
                    lbListaAerodromaFrom.Items.Add(aerodrom);
                }
            }
            if (lbListaAerodromaFrom.HasItems)
            {
                lbListaAerodromaFrom.Visibility = Visibility.Visible;
            }
            else if (!lbListaAerodromaFrom.HasItems || txtInputFrom.Text.Trim().Equals(string.Empty))
            {
                lbListaAerodromaFrom.Visibility = Visibility.Hidden;
            }
        }

        private void lbListaAerodromaFrom_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Aerodrom selektovaniAerodromFrom = (Aerodrom)lbListaAerodromaFrom.SelectedItem;
            if (selektovaniAerodromFrom != null)
            {
                lbListaAerodromaFrom.Visibility = Visibility.Hidden;
                txtInputFrom.Text = selektovaniAerodromFrom.ToString().Trim();
            }
        }

        private void txtInputTo_KeyUp(object sender, KeyEventArgs e)
        {
            lbListaAerodromaTo.Items.Clear();
            string aerodromInput = txtInputTo.Text.Trim();

            foreach (var aerodrom in Data.Instance.Aerodromi)
            {
                if (aerodrom.ToString().ToLower().Contains(aerodromInput.ToLower()) && aerodrom.Active)
                {
                    lbListaAerodromaTo.Items.Add(aerodrom);
                }
            }
            if (lbListaAerodromaTo.HasItems)
            {
                lbListaAerodromaTo.Visibility = Visibility.Visible;
            }
            else if (!lbListaAerodromaTo.HasItems || txtInputTo.Text.Trim().Equals(string.Empty))
            {
                lbListaAerodromaTo.Visibility = Visibility.Hidden;
            }
        }

        private void txtInputTo_LostFocus(object sender, RoutedEventArgs e)
        {
            lbListaAerodromaTo.Visibility = Visibility.Hidden;
        }

        private void lbListaAerodromaTo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Aerodrom selektovaniAerodromTo = (Aerodrom)lbListaAerodromaTo.SelectedItem;
            if (selektovaniAerodromTo != null)
            {
                lbListaAerodromaTo.Visibility = Visibility.Hidden;
                txtInputTo.Text = selektovaniAerodromTo.ToString().Trim();
            }
        }

        private void txtInputFrom_GotFocus(object sender, RoutedEventArgs e)
        {
            if(txtInputFrom.Text.Trim() == String.Empty)
            {
                lbListaAerodromaFrom.Visibility = Visibility.Visible;
            }
            
        }

        private void txtInputTo_GotFocus(object sender, RoutedEventArgs e)
        {
            if(txtInputTo.Text.Trim() == String.Empty)
            {
                lbListaAerodromaTo.Visibility = Visibility.Visible;
            }
            
           
        }

        private void btnPretrazi_Click(object sender, RoutedEventArgs e)
        {
            if(!txtInputFrom.Text.Trim().Equals(String.Empty) || !txtInputTo.Text.Trim().Equals(String.Empty))
            {
                Aerodrom polaziste = lbListaAerodromaFrom.SelectedItem as Aerodrom;
                Aerodrom odrediste = lbListaAerodromaTo.SelectedItem as Aerodrom;
                EKlasaSedista klasaSedista;
                if(rbEkonomska.IsChecked.Value)
                {
                    klasaSedista = EKlasaSedista.EKONOMSKA;
                }
                else
                {
                    klasaSedista = EKlasaSedista.BIZNIS;
                }
                if(rbJednosmerna.IsChecked.Value)
                {
                    tipKarte = ETipKarte.JEDNOSMERNA;
                }
                if(rbPovratna.IsChecked.Value)
                {
                    tipKarte = ETipKarte.POVRATNA;
                }
                if(rbPovratna.IsChecked == true)
                {
                    
                    try
                    {
                        DateTime polazak = dateDatumPolaska.SelectedDate.Value;
                        DateTime povratak = dateDatumPovratka.SelectedDate.Value;
                        List<Let> letovi = new List<Let>();

                        foreach (Let let in Data.Instance.Letovi)
                        {
                            if (let.Active)
                            {

                                if (let.Polaziste.Sifra.Equals(odrediste.Sifra) && let.Odrediste.Sifra.Equals(polaziste.Sifra))
                                {
                                    if (let.VremeDolaska > povratak)
                                    {
                                        letovi.Add(let);
                                    }
                                }
                            }

                        }

                        if(letovi.Count == 0)
                        {
                            MessageBox.Show("Ne postoje povratni letovi!\nSelektujte let u jednom pravcu");
                        }
                        else
                        {
                            _mainFrame.Content = new FiltriraniLetoviPage(_mainFrame, polaziste, odrediste, klasaSedista, polazak, povratak, tipKarte);

                        }

                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Selektujte datume!");
                    }
                }
                if (rbJednosmerna.IsChecked == true)
                {
                    try
                    {
                        DateTime polazak = dateDatumPolaska.SelectedDate.Value;
                        _mainFrame.Content = new FiltriraniLetoviPage(_mainFrame, polaziste, odrediste, klasaSedista, polazak, new DateTime(), tipKarte);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Selektujte datume!");
                    }
                }

            }
            else
            {
                MessageBox.Show("Unesite informacije o letu!");
            }
            
        }

        private void rbPovratna_Click(object sender, RoutedEventArgs e)
        {
            lblDatumPovratka.Visibility = Visibility.Visible;
            dateDatumPovratka.Visibility = Visibility.Visible;
        }

        private void rbJednosmerna_Click(object sender, RoutedEventArgs e)
        {
            lblDatumPovratka.Visibility = Visibility.Hidden;
            dateDatumPovratka.Visibility = Visibility.Hidden;
        }

        private void dateDatumPolaska_DateValidationError(object sender, DatePickerDateValidationErrorEventArgs e)
        {

        }
    }
}

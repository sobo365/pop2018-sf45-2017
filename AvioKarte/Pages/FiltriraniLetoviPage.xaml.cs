﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AvioKarte.Model;
using AvioKarte.Database;
using AvioKarte.Utility;

namespace AvioKarte.Pages
{
    /// <summary>
    /// Interaction logic for FiltriraniLetoviPage.xaml
    /// </summary>
    public partial class FiltriraniLetoviPage : Page
    {
        private Frame mainFrame;
        Aerodrom polaziste;
        Aerodrom odrediste;
        EKlasaSedista klasaSedista;
        DateTime datumPolaska;
        DateTime datumPovratka;
        int valueRed = 1;
        int valueKol = 1;
        string sediste;
        string statusSedista;
        ETipKarte tipKarte;
        Let selektovaniLet;
        public FiltriraniLetoviPage(Frame MainFrame, Aerodrom polaziste, Aerodrom odrediste, EKlasaSedista klasaSedista, DateTime datumPolaska, DateTime datumPovratka, ETipKarte tipKarte)
        {
            this.datumPolaska = datumPolaska;
            this.datumPovratka = datumPovratka;
            this.polaziste = polaziste;
            this.odrediste = odrediste;
            this.klasaSedista = klasaSedista;
            this.tipKarte = tipKarte;
            mainFrame = MainFrame;
            InitializeComponent();
            filtrirajListu();
            listFiltriranaLista.SelectedIndex = 0;
            generateBackground();
            initSeat();
            karte();
            praznaLista();

            selektovaniLet = listFiltriranaLista.SelectedItem as Let;

            
        }
        private void praznaLista()
        {
            if(Tool.Instance.kartaDoOdredista != null && listFiltriranaLista.Items.Count == 0)
            {
                if (MessageBox.Show("Ne postoje povratni letovi za selektovani datum, da li zelite da kupite kartu do odredista?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    Data.Instance.Karte.Add(Tool.Instance.kartaDoOdredista);
                    Tool.Instance.kartaDoOdredista = null;
                }
                else
                {
                    Tool.Instance.kartaDoOdredista = null;
                    return;
                }
            }
        }

       

        private void karte()
        {
            if (Tool.Instance.kartaDoOdredista == null)
            {
                if (tipKarte.Equals(ETipKarte.JEDNOSMERNA))
                {
                    btnKupi.Visibility = Visibility.Visible;
                    btnIzaberiPovratnu.Visibility = Visibility.Hidden;
                    btnKupiPovratnuKartu.Visibility = Visibility.Hidden;
                }

                if(tipKarte.Equals(ETipKarte.POVRATNA))
                {
                    btnIzaberiPovratnu.Visibility = Visibility.Visible;
                    btnKupi.Visibility = Visibility.Hidden;
                    btnKupiPovratnuKartu.Visibility = Visibility.Hidden;
                }
            }
            else
            {
                btnKupiPovratnuKartu.Visibility = Visibility.Visible;
                btnIzaberiPovratnu.Visibility = Visibility.Hidden;
                btnKupi.Visibility = Visibility.Hidden;
            }
        }

        private void initSeat()
        {
            
            Let selektovaniLet = listFiltriranaLista.SelectedItem as Let;
            foreach (Karta karta in Data.Instance.Karte)
            {
                if (selektovaniLet != null)
                {
                    if (klasaSedista.Equals(EKlasaSedista.BIZNIS))
                    {
                        foreach (Sediste s in selektovaniLet.Avion.ListaSedistaBiznis)
                        {
                            if (karta.Sediste.ToString().Equals(valueRed.ToString() + NtoC(valueKol)) && karta.Let.Avion == selektovaniLet.Avion && klasaSedista.Equals(karta.Sediste.KlasaSedista))
                            {
                                statusSedista = "Status: Zauzeto";
                                break;
                            }
                        }
                    }
                    if (klasaSedista.Equals(EKlasaSedista.EKONOMSKA))
                    {
                        foreach (Sediste s in selektovaniLet.Avion.ListaSedistaEkonomska)
                        {
                            if (karta.Sediste.ToString().Equals(valueRed.ToString() + NtoC(valueKol)) && karta.Let.Avion == selektovaniLet.Avion && klasaSedista.Equals(karta.Sediste.KlasaSedista))
                            {
                                statusSedista = "Status: Zauzeto";
                                break;
                            }
                        }
                    }

                }
            }

            lblStatusSedista.Content = statusSedista;
            
            
        }

       

        private void generateBackground()
        {
            if(selektovaniLet != null)
            {
                Avion avion = selektovaniLet.Avion;
           
            

            string message  = string.Empty;
            message += "\n\n";
            message += "\t";
            if(klasaSedista.Equals(EKlasaSedista.EKONOMSKA))
            {
                for (int i = 1; i <= avion.BrojKolonaSedistaE; i++)
                {
                    for (int j = 1; j <= avion.BrojRedovaSedistaE; j++)
                    {
                        message = message + j.ToString() + NtoC(i) + "\t";
                    }
                    message = message + "\n" + "\t";
                }
                lblBackground.Content = message;
            }
            if(klasaSedista.Equals(EKlasaSedista.BIZNIS))
            {
                for (int i = 1; i <= avion.BrojKolonaSedistaB; i++)
                {
                    for (int j = 1; j <= avion.BrojRedovaSedistaB; j++)
                    {
                        message = message + j.ToString() + NtoC(i) + "\t";
                    }
                    message = message + "\n" + "\t";
                }
                lblBackground.Content = message;
            }
            }
        }

        /*
        private void generateBackground()
        {
            Let selektovaniLet = listFiltriranaLista.SelectedItem as Let;
            Avion avion = selektovaniLet.Avion;

            string message = string.Empty;
            message += "\n";
            for (int i = 0; i < avion.ListaSedistaEkonomska.Count; i++)
            {
                if (i % avion.BrojKolonaSedistaE == 0)
                {

                    message += "\n";
                    message += "\t";
                }
                message = message + avion.ListaSedistaEkonomska[i] + "\t";

            }
            lblBackground.Content = message;
        }

            */

        private void filtrirajListu()
        {
            /*foreach(Let let in Data.Instance.Letovi)
            {
                if(let.Active)
                {
                    if (Tool.Instance.kartaDoOdredista == null)
                    {
                        if (let.Polaziste.Sifra.Equals(polaziste.Sifra) && let.Odrediste.Sifra.Equals(odrediste.Sifra))
                        {
                            if (let.VremePolaska > datumPolaska)
                            {
                                listFiltriranaLista.Items.Add(let);
                            }
                        }
                    }
                    else
                    {
                        if (let.Polaziste.Sifra.Equals(odrediste.Sifra) && let.Odrediste.Sifra.Equals(polaziste.Sifra) && Tool.Instance.kartaDoOdredista.Let.Aviokompanija.Id.Equals(let.Aviokompanija.Id))
                        {
                            if (let.VremeDolaska > datumPovratka)
                            {
                                listFiltriranaLista.Items.Add(let);
                            }
                        }
                    }
                }              
            }*/

            if (Tool.Instance.kartaDoOdredista == null)
            {
                IEnumerable<Let> filter = from let1 in Data.Instance.Letovi
                                         orderby let1.CenaKarte ascending
                                         where let1.Active == true && let1.Polaziste.Sifra == polaziste.Sifra && let1.Odrediste.Sifra == odrediste.Sifra
                                         && let1.VremePolaska > datumPolaska
                                         select let1;

                listFiltriranaLista.ItemsSource = filter;
            }
            else
            {
                IEnumerable<Let> filter = from let1 in Data.Instance.Letovi
                                          orderby let1.CenaKarte ascending
                                          where let1.Active == true && let1.Polaziste.Sifra == odrediste.Sifra && let1.Odrediste.Sifra == polaziste.Sifra &&
                                          Tool.Instance.kartaDoOdredista.Let.Aviokompanija.Id == let1.Aviokompanija.Id
                                          && let1.VremeDolaska > datumPovratka
                                          select let1;

                listFiltriranaLista.ItemsSource = filter;
            }

            

        }

        private void btnNazad_Click(object sender, RoutedEventArgs e)
        {
            mainFrame.Content = new LetoviPage(mainFrame);
        }

        private void listFiltriranaLista_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           
            selektovaniLet = listFiltriranaLista.SelectedItem as Let;
            if(selektovaniLet != null )
            {
                if (klasaSedista.Equals(EKlasaSedista.BIZNIS))
                {
                    lblCenaKarte.Content = selektovaniLet.CenaKarte * 1.5;
                    lblKlasaSedista.Content = "Biznis";
                    sliderKolona.Maximum = selektovaniLet.Avion.BrojKolonaSedistaB;
                    sliderRed.Maximum = selektovaniLet.Avion.BrojRedovaSedistaB;
                }
                if (klasaSedista.Equals(EKlasaSedista.EKONOMSKA))
                {
                    lblKlasaSedista.Content = "Ekonomska";
                    sliderKolona.Maximum = selektovaniLet.Avion.BrojKolonaSedistaE;
                    sliderRed.Maximum = selektovaniLet.Avion.BrojRedovaSedistaE;
                }
                generateBackground();
                initSeat();
            }
            


        }

        /*
         private void sliderKolona_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
         {
             sediste = "";
             for (int j = 0; j <= valueRed; j++)
             {
                 sediste += "\n";
             }
             valueKol = (int) sliderKolona.Value;
             for (int i = 1; i <= valueKol; i++)
             {
                 sediste += "\t";
             }
             lblPrikazSedista.Content = sediste + valueRed.ToString() + NtoC(valueKol);

         }

        private void sliderRed_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            sediste = "";

            valueRed = (int)sliderRed.Value;
            for (int i = 0; i <= valueRed; i++)
            {
                sediste += "\n";
            }
            for (int j = 1; j <= valueKol; j++)
            {
                sediste += "\t";
            }
            lblPrikazSedista.Content = sediste + valueRed.ToString() + NtoC(valueKol);
        }*/
        

        private void sliderKolona_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //Let selektovaniLet = listFiltriranaLista.SelectedItem as Let;
            valueKol = (int)sliderKolona.Value;
            sediste = "";
            statusSedista = "Status: Slobodno";
            for (int j = 0; j <= valueKol; j++)
            {
                sediste += "\n";
            }
            
            for (int i = 1; i <= valueRed; i++)
            {
                sediste += "\t";
            }
            lblPrikazSedista.Content = sediste + valueRed.ToString() + NtoC(valueKol);

            foreach(Karta karta in Data.Instance.Karte)
            {
                if(selektovaniLet != null && selektovaniLet.Id.Equals(karta.Let.Id))
                {
                    if(klasaSedista.Equals(EKlasaSedista.BIZNIS))
                    {
                        foreach(Sediste s in selektovaniLet.Avion.ListaSedistaBiznis)
                        {
                            if (karta.Sediste.ToString().Equals(valueRed.ToString() + NtoC(valueKol)) && karta.Let.Avion == selektovaniLet.Avion && klasaSedista.Equals(karta.Sediste.KlasaSedista))
                            {
                                statusSedista = "Status: Zauzeto";
                                break;
                            }
                        }
                    }
                    if (klasaSedista.Equals(EKlasaSedista.EKONOMSKA))
                    {
                        foreach (Sediste s in selektovaniLet.Avion.ListaSedistaEkonomska)
                        {
                            if (karta.Sediste.ToString().Equals(valueRed.ToString() + NtoC(valueKol)) && karta.Let.Avion == selektovaniLet.Avion && klasaSedista.Equals(karta.Sediste.KlasaSedista))
                            {
                                statusSedista = "Status: Zauzeto";
                                break;
                            }
                        }
                    }

                }
                

            }
            lblStatusSedista.Content = statusSedista;

        }

        private void sliderRed_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
           // Let selektovaniLet = listFiltriranaLista.SelectedItem as Let;
            sediste = "";
            statusSedista = "Status: Slobodno";

            for (int i = 0; i <= valueKol; i++)
            {
                sediste += "\n";
            }
            valueRed = (int)sliderRed.Value;
            for (int j = 1; j <= valueRed; j++)
            {
                sediste += "\t";
            }
            lblPrikazSedista.Content = sediste + valueRed.ToString() + NtoC(valueKol);

            foreach (Karta karta in Data.Instance.Karte)
            {
                if (selektovaniLet != null && selektovaniLet.Id.Equals(karta.Let.Id))
                {
                    if (klasaSedista.Equals(EKlasaSedista.BIZNIS))
                    {
                        foreach (Sediste s in selektovaniLet.Avion.ListaSedistaBiznis)
                        {
                            if (karta.Sediste.ToString().Equals(valueRed.ToString() + NtoC(valueKol)) && karta.Let.Avion == selektovaniLet.Avion && klasaSedista.Equals(karta.Sediste.KlasaSedista))
                            {
                                statusSedista = "Status: Zauzeto";
                                break;
                            }
                        }
                    }
                    if (klasaSedista.Equals(EKlasaSedista.EKONOMSKA))
                    {
                        foreach (Sediste s in selektovaniLet.Avion.ListaSedistaEkonomska)
                        {
                            if (karta.Sediste.ToString().Equals(valueRed.ToString() + NtoC(valueKol)) && karta.Let.Avion == selektovaniLet.Avion && klasaSedista.Equals(karta.Sediste.KlasaSedista))
                            {
                                statusSedista = "Status: Zauzeto";
                                break;
                            }
                        }
                    }

                }


            }

            lblStatusSedista.Content = statusSedista;
        }

    

        private char NtoC(int num)
        {
            char[] alphabet = "abcdefghijklmnopqrstuvwxyz".ToUpper().ToCharArray();
            return alphabet[num - 1];
        }

        private void btnKupi_Click(object sender, RoutedEventArgs e)
        {
            if(!statusSedista.Equals("Status: Zauzeto"))
            {
                if (Tool.Instance.loggedUser != null)
                {
                    Let selektovaniLet = listFiltriranaLista.SelectedItem as Let;
                    if (selektovaniLet != null)
                    {
                        foreach (Sediste s in Data.Instance.Letovi[Tool.Instance.indexLeta(selektovaniLet.BrojLeta)].Avion.ListaSedistaEkonomska)
                        {
                            if (s.ToString().Equals(valueRed.ToString() + NtoC(valueKol)))
                            {
                                Karta karta = new Karta(selektovaniLet, s, Tool.Instance.loggedUser);
                                if(klasaSedista.Equals(EKlasaSedista.BIZNIS))
                                {
                                    karta.Cena = selektovaniLet.CenaKarte * 1.5;
                                    karta.KlasaSedista = EKlasaSedista.BIZNIS;
                                }
                                else
                                {
                                    karta.Cena = selektovaniLet.CenaKarte;
                                    karta.KlasaSedista = EKlasaSedista.EKONOMSKA;
                                }
                                karta.populateEntities();
                                
                                karta.Insert();
                                Data.Instance.UcitajKarteDB();
                                statusSedista = "Status: Zauzeto";
                                lblStatusSedista.Content = statusSedista;
                                KartaDetaljnoWindow kdw = new KartaDetaljnoWindow(karta);
                                kdw.Show();
                                mainFrame.Content = new PutnikPocetnaPage();
                                break;
                                
                            }
                        }

                    }
                }
                else
                {
                    Let selektovaniLet = listFiltriranaLista.SelectedItem as Let;
                    if (selektovaniLet != null)
                    {
                        foreach (Sediste s in Data.Instance.Letovi[Tool.Instance.indexLeta(selektovaniLet.BrojLeta)].Avion.ListaSedistaEkonomska)
                        {
                            if (s.ToString().Equals(valueRed.ToString() + NtoC(valueKol)))
                            {
                                Karta karta = new Karta(selektovaniLet, s, Tool.Instance.loggedUser);
                                if(klasaSedista.Equals(EKlasaSedista.BIZNIS))
                                {
                                    karta.Cena = selektovaniLet.CenaKarte * 1.5;
                                    karta.KlasaSedista = EKlasaSedista.BIZNIS;
                                }
                                else
                                {
                                    karta.Cena = selektovaniLet.CenaKarte;
                                    karta.KlasaSedista = EKlasaSedista.EKONOMSKA;
                                }
                                UnosPodataka up = new UnosPodataka(selektovaniLet, false, karta);
                                up.Show();

                                statusSedista = "Status: Zauzeto";
                                lblStatusSedista.Content = statusSedista;
                                mainFrame.Content = new PutnikPocetnaPage();
                                break;
                            }
                        }

                    }
     
                }
            }
            else
            {
                MessageBox.Show(valueRed.ToString() + NtoC(valueKol) + " je zauzeto!");
            }
            
            
            
            
        }

        private void btnSedista_Click(object sender, RoutedEventArgs e)
        {
            if(listFiltriranaLista.SelectedItem != null)
            {
                sliderKolona.Value = 1;
                sliderRed.Value = 1;

                colListaLetova.Width = new GridLength(0);
                colKarta.Width = new GridLength(0);
                colSedista.Width = new GridLength(1, GridUnitType.Star);
            }
            
        }

        private void btnKarta_Click(object sender, RoutedEventArgs e)
        {
            valueRed = 1;
            valueKol = 1;
            generateBackground();
            initSeat();

            colListaLetova.Width = new GridLength(1, GridUnitType.Star);
            colKarta.Width = new GridLength(1,GridUnitType.Star);
            colSedista.Width = new GridLength(0);
        }

        private void btnIzaberiPovratnu_Click(object sender, RoutedEventArgs e)
        {
            if (!statusSedista.Equals("Status: Zauzeto"))
            {
                if (Tool.Instance.loggedUser != null)
                {
                    Let selektovaniLet = listFiltriranaLista.SelectedItem as Let;
                    if (selektovaniLet != null)
                    {
                        foreach (Sediste s in Data.Instance.Letovi[Tool.Instance.indexLeta(selektovaniLet.BrojLeta)].Avion.ListaSedistaEkonomska)
                        {
                            if (s.ToString().Equals(valueRed.ToString() + NtoC(valueKol)))
                            {
                                Karta karta = new Karta(selektovaniLet, s, Tool.Instance.loggedUser);
                                
                                Tool.Instance.kartaDoOdredista = karta;
                                if (klasaSedista.Equals(EKlasaSedista.BIZNIS))
                                {
                                    karta.Cena = selektovaniLet.CenaKarte * 1.5;
                                    karta.KlasaSedista = EKlasaSedista.BIZNIS;
                                }
                                else
                                {
                                    karta.Cena = selektovaniLet.CenaKarte;
                                    karta.KlasaSedista = EKlasaSedista.EKONOMSKA;
                                }
                                statusSedista = "Status: Zauzeto";
                                lblStatusSedista.Content = statusSedista;
                                KartaDetaljnoWindow kdw = new KartaDetaljnoWindow(karta);
                                kdw.Show();
                                MessageBox.Show("Kupili ste kartu do odredista, bicete preusmereni na stranicu za odabir povratnog leta!");
                                mainFrame.Content = new FiltriraniLetoviPage(mainFrame, polaziste, odrediste, klasaSedista, new DateTime(), datumPovratka, tipKarte);
                                
                                break;
                            }
                        }

                    }
                }
                else
                {
                    Let selektovaniLet = listFiltriranaLista.SelectedItem as Let;
                    if (selektovaniLet != null)
                    {
                        foreach (Sediste s in Data.Instance.Letovi[Tool.Instance.indexLeta(selektovaniLet.BrojLeta)].Avion.ListaSedistaEkonomska)
                        {
                            if (s.ToString().Equals(valueRed.ToString() + NtoC(valueKol)))
                            {
                                Karta karta = new Karta(selektovaniLet, s, new Korisnik());
                                Tool.Instance.kartaDoOdredista = karta;
                                if (klasaSedista.Equals(EKlasaSedista.BIZNIS))
                                {
                                    karta.Cena = selektovaniLet.CenaKarte * 1.5;
                                    karta.KlasaSedista = EKlasaSedista.BIZNIS;
                                }
                                else
                                {
                                    karta.Cena = selektovaniLet.CenaKarte;
                                    karta.KlasaSedista = EKlasaSedista.EKONOMSKA;
                                }
                                UnosPodataka up = new UnosPodataka(selektovaniLet, true, karta);
                                up.Show();     
                                statusSedista = "Status: Zauzeto";
                                lblStatusSedista.Content = statusSedista;
                                mainFrame.Content = new FiltriraniLetoviPage(mainFrame, polaziste, odrediste, klasaSedista, new DateTime(), datumPovratka, ETipKarte.POVRATNA);
                                filtrirajListu();
                                break;
                            }
                        }
                        
                    }
                }
            }
            else
            {
                MessageBox.Show(valueRed.ToString() + NtoC(valueKol) + " je zauzeto!");
            }
            
        }

        private void btnKupiPovratnuKartu_Click(object sender, RoutedEventArgs e)
        {
            
            if (!statusSedista.Equals("Status: Zauzeto"))
            {
                if (Tool.Instance.loggedUser != null)
                {
                    Let selektovaniLet = listFiltriranaLista.SelectedItem as Let;
                    if (selektovaniLet != null)
                    {
                        foreach (Sediste s in Data.Instance.Letovi[Tool.Instance.indexLeta(selektovaniLet.BrojLeta)].Avion.ListaSedistaEkonomska)
                        {
                            if (s.ToString().Equals(valueRed.ToString() + NtoC(valueKol)))
                            {
                                Karta karta = new Karta(selektovaniLet, s, Tool.Instance.loggedUser);
                                
                                karta.populateEntities();
                                Tool.Instance.kartaDoOdredista.populateEntities();
                                if (klasaSedista.Equals(EKlasaSedista.BIZNIS))
                                {
                                    karta.Cena = selektovaniLet.CenaKarte * 1.5;
                                    karta.KlasaSedista = EKlasaSedista.BIZNIS;
                                }
                                else
                                {
                                    karta.Cena = selektovaniLet.CenaKarte;
                                    karta.KlasaSedista = EKlasaSedista.EKONOMSKA;
                                }
                                karta.Insert();
                                Tool.Instance.kartaDoOdredista.Insert();                                
                                Data.Instance.UcitajKarteDB();
                                statusSedista = "Status: Zauzeto";
                                lblStatusSedista.Content = statusSedista;
                                KartaDetaljnoWindow kdw = new KartaDetaljnoWindow(karta);
                                kdw.Show();
                                mainFrame.Content = new PutnikPocetnaPage();
                                Tool.Instance.kartaDoOdredista = null;
                                break;
                            }
                        }

                    }
                }
                else
                {
                    Let selektovaniLet = listFiltriranaLista.SelectedItem as Let;
                    if (selektovaniLet != null)
                    {
                        foreach (Sediste s in Data.Instance.Letovi[Tool.Instance.indexLeta(selektovaniLet.BrojLeta)].Avion.ListaSedistaEkonomska)
                        {
                            if (s.ToString().Equals(valueRed.ToString() + NtoC(valueKol)))
                            {
                                Karta karta = new Karta(selektovaniLet, s, new Korisnik());
                                
                                karta.Ime = Tool.Instance.kartaDoOdredista.Ime;
                                karta.Prezime = Tool.Instance.kartaDoOdredista.Prezime;
                                karta.Adresa = Tool.Instance.kartaDoOdredista.Adresa;
                                karta.Pol = Tool.Instance.kartaDoOdredista.Pol;
                                karta.Email = Tool.Instance.kartaDoOdredista.Email;
                                if (klasaSedista.Equals(EKlasaSedista.BIZNIS))
                                {
                                    karta.Cena = selektovaniLet.CenaKarte * 1.5;
                                    karta.KlasaSedista = EKlasaSedista.BIZNIS;
                                }
                                else
                                {
                                    karta.Cena = selektovaniLet.CenaKarte;
                                    karta.KlasaSedista = EKlasaSedista.EKONOMSKA;
                                }
                                karta.Insert();
                                Tool.Instance.kartaDoOdredista.Insert();
                                Data.Instance.UcitajKarteDB();
                                statusSedista = "Status: Zauzeto";
                                lblStatusSedista.Content = statusSedista;
                                KartaDetaljnoWindow kdw = new KartaDetaljnoWindow(karta);
                                kdw.Show();
                                mainFrame.Content = new PutnikPocetnaPage();
                                Tool.Instance.kartaDoOdredista = null;
                                break;
                            }
                        }

                    }
                }
            }
            else
            {
                MessageBox.Show(valueRed.ToString() + NtoC(valueKol) + " je zauzeto!");
            }
            

        }
    }
}

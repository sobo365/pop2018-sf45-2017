﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AvioKarte.Database;
using AvioKarte.Model;
using AvioKarte.Utility;

namespace AvioKarte.Pages
{
    /// <summary>
    /// Interaction logic for MojProfilPutnikPage.xaml
    /// </summary>
    public partial class MojProfilPutnikPage : Page
    {
        ICollectionView v;
        public MojProfilPutnikPage()
        {
            InitializeComponent();
            this.DataContext = Tool.Instance.loggedUser;
            UcitajKarte();
        }

        private void UcitajKarte()
        {
            foreach(Karta k in Data.Instance.Karte)
            {
                if(k.Putnik != null)
                {
                    if (Tool.Instance.loggedUser.KorisnickoIme.Equals(k.Putnik.KorisnickoIme))
                    {
                        listKarte.Items.Add(k);
                    }
                }
                
            }
        }

        private void btnIzmena_Click(object sender, RoutedEventArgs e)
        {
            
            Korisnik clone = Tool.Instance.loggedUser.Clone() as Korisnik;
            EditKorisniciWindow ekw = new EditKorisniciWindow(clone, EditKorisniciWindow.Opcija.KORISNIK_IZMENA);
            ekw.Show();
        }

        private void btnDetaljno_Click(object sender, RoutedEventArgs e)
        {
            Karta karta = listKarte.SelectedItem as Karta;
            if(karta != null)
            {
                KartaDetaljnoWindow kdw = new KartaDetaljnoWindow(karta);
                kdw.Show();
            }
            
        }
    }
}

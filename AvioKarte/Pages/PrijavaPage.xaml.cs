﻿using AvioKarte.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AvioKarte.Model;
using AvioKarte.Utility;

namespace AvioKarte.Pages
{
    /// <summary>
    /// Interaction logic for PrijavaPage.xaml
    /// </summary>
    public partial class PrijavaPage : Page
    {
        private Window _currentWindow; 
        public PrijavaPage(Window currentWindow)
        {
            _currentWindow = currentWindow;
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {

            string korisnickoIme = txtKorisnickoIme.Text.Trim();
            string lozinka = txtLozinka.Password.Trim();

            string greska = String.Empty;

            if (korisnickoIme.Equals(String.Empty))
            {
                lblMessage.Content = "Niste uneli korisnicko ime!";
            }
            else if (lozinka.Equals(String.Empty))
            {
                lblMessage.Content = "Niste uneli lozinku!";
            }
            else
            {
                foreach (var korisnik in Data.Instance.Korisnici)
                {
                    if (korisnik.KorisnickoIme.Equals(korisnickoIme) && korisnik.Lozinka.Equals(lozinka))
                    {
                        if(korisnik.TipKorisnika == ETipKorisnika.PUTNIK)
                        {
                            Tool.Instance.loggedUser = korisnik;
                            PutnikWindow pw = new PutnikWindow();
                            _currentWindow.Close();
                            pw.Show();
                            break;
                        }
                        else if(korisnik.TipKorisnika == ETipKorisnika.ADMINISTRATOR)
                        {
                            Tool.Instance.loggedUser = korisnik;
                            AdminWindow aw = new AdminWindow();
                            _currentWindow.Close();
                            aw.Show();
                            break;
                        }
                        
                    }
                    lblMessage.Content = "Nepostojeci korisnik";
                }
            }

            
            
        }
    }
}

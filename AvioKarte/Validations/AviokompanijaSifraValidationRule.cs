﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace AvioKarte.Validations
{
    class AviokompanijaSifraValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            foreach(var aviokompanija in Database.Data.Instance.Aviokompanije)
            {
                if(value.ToString().ToLower().Equals(aviokompanija.Sifra.ToLower()))
                {
                    return new ValidationResult(false, "Ova sifra se vec koristi");
                }
            }
            return new ValidationResult(true, "OK");
        }
    }
}

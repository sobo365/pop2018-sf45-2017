﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace AvioKarte.Validations
{
    class EmptyFieldValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if(value != null)
            {
                if (value.ToString().Equals(string.Empty))
                {
                    return new ValidationResult(false, "Empty field");
                }
            }           
           
            return new ValidationResult(true, "OK!");
        }
    }
}

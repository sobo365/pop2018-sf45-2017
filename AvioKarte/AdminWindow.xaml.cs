﻿using AvioKarte.Database;
using AvioKarte.Model;
using AvioKarte.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AvioKarte
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        public AdminWindow()
        {
            InitializeComponent();
        }

        private void BtnAerodromi_Click(object sender, RoutedEventArgs e)
        {
            AerodromiWindow aw = new AerodromiWindow();
            aw.Show();

        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void menuLogout_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnKorisnici_Click(object sender, RoutedEventArgs e)
        {
            KorisniciWindow kw = new KorisniciWindow();
            kw.Show();

        }

        private void btnLetovi_Click(object sender, RoutedEventArgs e)
        {
            LetoviWindow lw = new LetoviWindow();
            lw.Show();
        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mw = new MainWindow();
            Tool.Instance.loggedUser = null;
            this.Close();
            mw.Show();
        }

        private void btnAviokompanije_Click(object sender, RoutedEventArgs e)
        {
            AvioKompanijeWindow akw = new AvioKompanijeWindow();
            akw.Show();
        }

        private void btnAvioni_Click(object sender, RoutedEventArgs e)
        {
            AvioniWindow aw = new AvioniWindow();
            aw.Show();
        }

        private void btnKarte_Click(object sender, RoutedEventArgs e)
        {
            KarteWindow kw = new KarteWindow();
            kw.Show();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AvioKarte.Database;
using AvioKarte.Model;
using AvioKarte.Utility;

namespace AvioKarte
{
    /// <summary>
    /// Interaction logic for AvioniWindow.xaml
    /// </summary>
    public partial class AvioniWindow : Window
    {
        ICollectionView view;
        public AvioniWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.Avioni);
            view.Filter = Filter;
            lbListaAviona.IsSynchronizedWithCurrentItem = true;
            lbListaAviona.ItemsSource = Data.Instance.Avioni;
        }

        private bool Filter(object obj)
        {
            Avion avion = obj as Avion;
            if(txtPretragaNazivAviona.Text.Equals(String.Empty) && txtPretragaAviokompanija.Text.Equals(String.Empty))
            {
                return avion.Active;
            }
            else
            {
                return avion.Active && avion.NazivAviona.ToLower().Contains(txtPretragaNazivAviona.Text.ToLower()) && avion.Aviokompanija.Naziv.ToLower().Contains(txtPretragaAviokompanija.Text.ToLower());
            }
        }

        private void BtnDodajAvion_Click(object sender, RoutedEventArgs e)
        {
            EditAvioniWindow eaw = new EditAvioniWindow(new Avion(), EditAvioniWindow.Opcija.DODAVANJE);
            eaw.Show();
        }

        private void BtnIzmeniAvion_Click(object sender, RoutedEventArgs e)
        {
            Avion avion = lbListaAviona.SelectedItem as Avion;

            if(avion != null)
            {
                Avion clone = avion.Clone() as Avion;
                EditAvioniWindow eaw = new EditAvioniWindow(clone, EditAvioniWindow.Opcija.IZMENA);
                eaw.Show();
            }
            else
            {
                MessageBox.Show("Selektujte avion!");
            }
        }

        private void BtnObrisiAvion_Click(object sender, RoutedEventArgs e)
        {
            Avion selektovaniAvion = lbListaAviona.SelectedItem as Avion;
            if(selektovaniAvion != null)
            {
                if (MessageBox.Show($"Da li zelite da obrisete avion: {selektovaniAvion}", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    selektovaniAvion.Delete();
                    Data.Instance.UcitajAvioneDB();
                }
            }
            else
            {
                MessageBox.Show("Selektujte avion");
            }
            view.Refresh();
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnPrikaziBiznisSedista_Click(object sender, RoutedEventArgs e)
        {
            Avion avion = lbListaAviona.SelectedItem as Avion;
            string message = string.Empty;
            if (avion != null)
            {
                message = avion.NazivAviona + "\t BIZNIS KLASA" + "\n\n\n\n";
                for (int i = 0; i < avion.ListaSedistaBiznis.Count; i++)
                {
                    if (i % avion.BrojKolonaSedistaB == 0)
                    {
                        message += "\n";
                    }
                    message = message + avion.ListaSedistaBiznis[i] + "\t";

                }
                MessageBox.Show(message);
            }
            else
            {
                MessageBox.Show("Selektujte avion!");
            }
            
        } 

        private void btnPrikaziEkonomskaSedista_Click(object sender, RoutedEventArgs e)
        {
            Avion avion = lbListaAviona.SelectedItem as Avion;
            string message = string.Empty;
            if (avion != null)
            {
                message = avion.NazivAviona + "\t EKONOMSKA KLASA" + "\n\n\n\n";
                for (int i = 0; i < avion.ListaSedistaEkonomska.Count; i++)
                {
                    if (i % avion.BrojKolonaSedistaE == 0)
                    {
                        message += "\n";
                    }
                    message = message + avion.ListaSedistaEkonomska[i] + "\t";
                    
                }
                MessageBox.Show(message);
            }
            else
            {
                MessageBox.Show("Selektujte avion!");
            }
            
        }

        private void txtPonistiPretragu_Click(object sender, RoutedEventArgs e)
        {
            txtPretragaNazivAviona.Text = String.Empty;
            txtPretragaAviokompanija.Text = String.Empty;
            view.Refresh();
        }

        private void txtPretragaNazivAviona_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void txtPretragaAviokompanija_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}

﻿#pragma checksum "..\..\AerodromiWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "DAE4258C75DC10B67D56AB213C57D62A958F44CF"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using AvioKarte;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AvioKarte {
    
    
    /// <summary>
    /// AerodromiWindow
    /// </summary>
    public partial class AerodromiWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 39 "..\..\AerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox lbListaAerodroma;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\AerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSifraAerodromaText;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\AerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSifraAerodroma;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\AerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblNazivAerodromaText;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\AerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblNazivAerodroma;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\AerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblGradAerodromaText;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\AerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblGradAerodroma;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\AerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblBrojKapijaText;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\AerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblBrojKapija;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\AerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnDodajAerodrom;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\AerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnIzmeniAerodrom;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\AerodromiWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnObrisiAerodrom;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/AvioKarte;component/aerodromiwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\AerodromiWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 35 "..\..\AerodromiWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.lbListaAerodroma = ((System.Windows.Controls.ListBox)(target));
            return;
            case 3:
            this.lblSifraAerodromaText = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.lblSifraAerodroma = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.lblNazivAerodromaText = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.lblNazivAerodroma = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.lblGradAerodromaText = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.lblGradAerodroma = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.lblBrojKapijaText = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.lblBrojKapija = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.BtnDodajAerodrom = ((System.Windows.Controls.Button)(target));
            
            #line 54 "..\..\AerodromiWindow.xaml"
            this.BtnDodajAerodrom.Click += new System.Windows.RoutedEventHandler(this.BtnDodajAerodrom_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.BtnIzmeniAerodrom = ((System.Windows.Controls.Button)(target));
            
            #line 59 "..\..\AerodromiWindow.xaml"
            this.BtnIzmeniAerodrom.Click += new System.Windows.RoutedEventHandler(this.BtnIzmeniAerodrom_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.BtnObrisiAerodrom = ((System.Windows.Controls.Button)(target));
            
            #line 64 "..\..\AerodromiWindow.xaml"
            this.BtnObrisiAerodrom.Click += new System.Windows.RoutedEventHandler(this.BtnObrisiAerodrom_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}


﻿#pragma checksum "..\..\..\Pages\FiltriraniLetoviPage.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1724A46BA3F55EC6D756853D2644CAEF34FB4F2D"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using AvioKarte.Pages;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AvioKarte.Pages {
    
    
    /// <summary>
    /// FiltriraniLetoviPage
    /// </summary>
    public partial class FiltriraniLetoviPage : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 17 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ColumnDefinition colListaLetova;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ColumnDefinition colKarta;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ColumnDefinition colSedista;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblHeader0;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox listFiltriranaLista;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblHeader;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblOdText;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblOd;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblDoText;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblDo;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblVremePolaskaText;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblVremePolaska;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblVremeDolaskaText;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblVremeDolaska;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblCenaKarteText;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblCenaKarte;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblAviokompanijaText;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblAviokompanija;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblAvionText;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblAvion;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblKlasaSedistaText;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblKlasaSedista;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSedista;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblHeader2;
        
        #line default
        #line hidden
        
        
        #line 121 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblStatusSedista;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblBackground;
        
        #line default
        #line hidden
        
        
        #line 140 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblPrikazSedista;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider sliderKolona;
        
        #line default
        #line hidden
        
        
        #line 144 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider sliderRed;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnKarta;
        
        #line default
        #line hidden
        
        
        #line 158 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnKupi;
        
        #line default
        #line hidden
        
        
        #line 164 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnIzaberiPovratnu;
        
        #line default
        #line hidden
        
        
        #line 170 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnKupiPovratnuKartu;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/AvioKarte;component/pages/filtriraniletovipage.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.colListaLetova = ((System.Windows.Controls.ColumnDefinition)(target));
            return;
            case 2:
            this.colKarta = ((System.Windows.Controls.ColumnDefinition)(target));
            return;
            case 3:
            this.colSedista = ((System.Windows.Controls.ColumnDefinition)(target));
            return;
            case 4:
            this.lblHeader0 = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.listFiltriranaLista = ((System.Windows.Controls.ListBox)(target));
            
            #line 35 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
            this.listFiltriranaLista.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.listFiltriranaLista_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.lblHeader = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.lblOdText = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.lblOd = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.lblDoText = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.lblDo = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.lblVremePolaskaText = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.lblVremePolaska = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.lblVremeDolaskaText = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.lblVremeDolaska = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.lblCenaKarteText = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.lblCenaKarte = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.lblAviokompanijaText = ((System.Windows.Controls.Label)(target));
            return;
            case 18:
            this.lblAviokompanija = ((System.Windows.Controls.Label)(target));
            return;
            case 19:
            this.lblAvionText = ((System.Windows.Controls.Label)(target));
            return;
            case 20:
            this.lblAvion = ((System.Windows.Controls.Label)(target));
            return;
            case 21:
            this.lblKlasaSedistaText = ((System.Windows.Controls.Label)(target));
            return;
            case 22:
            this.lblKlasaSedista = ((System.Windows.Controls.Label)(target));
            return;
            case 23:
            this.btnSedista = ((System.Windows.Controls.Button)(target));
            
            #line 98 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
            this.btnSedista.Click += new System.Windows.RoutedEventHandler(this.btnSedista_Click);
            
            #line default
            #line hidden
            return;
            case 24:
            this.lblHeader2 = ((System.Windows.Controls.Label)(target));
            return;
            case 25:
            this.lblStatusSedista = ((System.Windows.Controls.Label)(target));
            return;
            case 26:
            this.lblBackground = ((System.Windows.Controls.Label)(target));
            return;
            case 27:
            this.lblPrikazSedista = ((System.Windows.Controls.Label)(target));
            return;
            case 28:
            this.sliderKolona = ((System.Windows.Controls.Slider)(target));
            
            #line 142 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
            this.sliderKolona.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.sliderKolona_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 29:
            this.sliderRed = ((System.Windows.Controls.Slider)(target));
            
            #line 144 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
            this.sliderRed.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.sliderRed_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 30:
            this.btnKarta = ((System.Windows.Controls.Button)(target));
            
            #line 152 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
            this.btnKarta.Click += new System.Windows.RoutedEventHandler(this.btnKarta_Click);
            
            #line default
            #line hidden
            return;
            case 31:
            this.btnKupi = ((System.Windows.Controls.Button)(target));
            
            #line 158 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
            this.btnKupi.Click += new System.Windows.RoutedEventHandler(this.btnKupi_Click);
            
            #line default
            #line hidden
            return;
            case 32:
            this.btnIzaberiPovratnu = ((System.Windows.Controls.Button)(target));
            
            #line 164 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
            this.btnIzaberiPovratnu.Click += new System.Windows.RoutedEventHandler(this.btnIzaberiPovratnu_Click);
            
            #line default
            #line hidden
            return;
            case 33:
            this.btnKupiPovratnuKartu = ((System.Windows.Controls.Button)(target));
            
            #line 170 "..\..\..\Pages\FiltriraniLetoviPage.xaml"
            this.btnKupiPovratnuKartu.Click += new System.Windows.RoutedEventHandler(this.btnKupiPovratnuKartu_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}


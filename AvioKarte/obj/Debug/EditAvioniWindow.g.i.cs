﻿#pragma checksum "..\..\EditAvioniWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "344D0F47B94CDAD9A782D8AE9B185089DE992246"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using AvioKarte;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AvioKarte {
    
    
    /// <summary>
    /// EditAvioniWindow
    /// </summary>
    public partial class EditAvioniWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 36 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblHeader;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSifraAviona;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSifraAviona;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblNazivAviona;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNazivAviona;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblBrojRedovaSedistaB;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBrojRedovaSedistaB;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblBrojKolonaSedistaB;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBrojKolonaSedistaB;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblBrojRedovaSedistaE;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBrojRedovaSedistaE;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblBrojKolonaSedistaE;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBrojKolonaSedistaE;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblAviokompanija;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbAviokompanija;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblErrorMessage;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSacuvaj;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\EditAvioniWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnOdustani;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/AvioKarte;component/editavioniwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\EditAvioniWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.lblHeader = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.lblSifraAviona = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.txtSifraAviona = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.lblNazivAviona = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.txtNazivAviona = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.lblBrojRedovaSedistaB = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.txtBrojRedovaSedistaB = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.lblBrojKolonaSedistaB = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.txtBrojKolonaSedistaB = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.lblBrojRedovaSedistaE = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.txtBrojRedovaSedistaE = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.lblBrojKolonaSedistaE = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.txtBrojKolonaSedistaE = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.lblAviokompanija = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.cbAviokompanija = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 16:
            this.lblErrorMessage = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.btnSacuvaj = ((System.Windows.Controls.Button)(target));
            
            #line 67 "..\..\EditAvioniWindow.xaml"
            this.btnSacuvaj.Click += new System.Windows.RoutedEventHandler(this.btnSacuvaj_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.btnOdustani = ((System.Windows.Controls.Button)(target));
            
            #line 72 "..\..\EditAvioniWindow.xaml"
            this.btnOdustani.Click += new System.Windows.RoutedEventHandler(this.btnOdustani_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}


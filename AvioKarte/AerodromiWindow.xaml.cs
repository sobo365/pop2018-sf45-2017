﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AvioKarte.Database;
using AvioKarte.Model;
using AvioKarte.Utility;

namespace AvioKarte
{
    /// <summary>
    /// Interaction logic for AerodromiWindow.xaml
    /// </summary>
    public partial class AerodromiWindow : Window
    {
        ICollectionView view;
        public AerodromiWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.Aerodromi);
            view.Filter = Filter;
            lbListaAerodroma.ItemsSource = Data.Instance.Aerodromi;
            lbListaAerodroma.IsSynchronizedWithCurrentItem = true;
            
        }
        
        private bool Filter(object obj)
        {
            
            Aerodrom aerodrom = obj as Aerodrom;
            return aerodrom.Active;
           

    } 

        private void BtnDodajAerodrom_Click(object sender, RoutedEventArgs e)
        {
            EditAerodromWindow eaw = new EditAerodromWindow(new Aerodrom(), EditAerodromWindow.Opcija.DODAVANJE);
            eaw.Show();
                        
        }

        private void BtnIzmeniAerodrom_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom aerodrom = lbListaAerodroma.SelectedItem as Aerodrom;
            if(aerodrom != null)
            {
                Aerodrom clone = aerodrom.Clone() as Aerodrom;
                EditAerodromWindow eaw = new EditAerodromWindow(clone, EditAerodromWindow.Opcija.IZMENA);
                eaw.Show();
            }
            else
            {
                MessageBox.Show("Selektujte aerodrom!");
            }
        }

        private void BtnObrisiAerodrom_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom aerodrom = (Aerodrom)lbListaAerodroma.SelectedItem;
            if (aerodrom != null && aerodrom.Active)
            {
                
                if (MessageBox.Show($"Da li zelite da obrisete aerodrom: {aerodrom}", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    aerodrom.Delete();
                    Data.Instance.UcitajAerodromeDB();
                    view.Refresh();
                }
            }
            else
            {
                MessageBox.Show("Selektujte aerodrom");
            }
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void txtPretrazi_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}

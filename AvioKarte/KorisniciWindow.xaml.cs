﻿using AvioKarte.Database;
using AvioKarte.Model;
using AvioKarte.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AvioKarte
{
    /// <summary>
    /// Interaction logic for KorisniciWindow.xaml
    /// </summary>
    public partial class KorisniciWindow : Window
    {
        ICollectionView view;
        public KorisniciWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.Korisnici.Where(K => K.Active));
            view.Filter = Filter;
            listKorisnici.IsSynchronizedWithCurrentItem = true;
            listKorisnici.ItemsSource = Data.Instance.Korisnici.Where(K => K.Active);
        }

        private bool Filter(object obj)
        {
            Korisnik korisnik = obj as Korisnik;

            return korisnik.Active;
                       
        }

        private void btnDodajKorisnika_Click(object sender, RoutedEventArgs e)
        {
           EditKorisniciWindow ekw = new EditKorisniciWindow(new Korisnik(), EditKorisniciWindow.Opcija.DODAVANJE);
           

            if (ekw.ShowDialog() == false)
            {
                IEnumerable<Korisnik> query = from korisnik in Data.Instance.Korisnici
                                              orderby korisnik.Ime
                                              where korisnik.Active == true
                                              select korisnik;

                listKorisnici.ItemsSource = query;
                view.Refresh();
            }
        }

        private void btnIzmeniKorisnika_Click(object sender, RoutedEventArgs e)
        {
            Korisnik selektovaniKorisnik = listKorisnici.SelectedItem as Korisnik;
            if (selektovaniKorisnik != null)
            {
                Korisnik clone = selektovaniKorisnik.Clone() as Korisnik;
                EditKorisniciWindow ekw = new EditKorisniciWindow(clone, EditKorisniciWindow.Opcija.IZMENA);
                if (ekw.ShowDialog() == false)
                {
                    IEnumerable<Korisnik> query = from korisnik in Data.Instance.Korisnici
                                                  orderby korisnik.Ime
                                                  where korisnik.Active == true
                                                  select korisnik;

                    listKorisnici.ItemsSource = query;
                    view.Refresh();
                }
            }
            else
            {
                MessageBox.Show("Selektujte korisnika!");
            }
        }

        private void btnObrisiKorisnika_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = listKorisnici.SelectedItem as Korisnik;
            if(korisnik != null && korisnik.Active)
            {
                if(MessageBox.Show($"Da li zelite da obrisete korisnika: {korisnik}", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    korisnik.Delete();
                    Data.Instance.UcitajKorisnikeDB();

                    IEnumerable<Korisnik> query = from k in Data.Instance.Korisnici
                                                  orderby k.Ime
                                                  where k.Active == true
                                                  select k;

                    listKorisnici.ItemsSource = query;
                    view.Refresh();
                }
                
            }
            else
            {
                MessageBox.Show("Selektujte korisnika!");
            }
            view.Refresh();

        }
        private void btnIzlaz_Click(object sender, RoutedEventArgs e)
        {

            this.Close();
        }

        private void txtPretragaIme_KeyUp(object sender, KeyEventArgs e)
        {
            txtPretragaPrezime.Text = string.Empty;
            txtPretragaKorisnickoIme.Text = string.Empty;
            IEnumerable<Korisnik> query = from korisnik in Data.Instance.Korisnici
                                          orderby korisnik.Ime
                                          where korisnik.Ime == txtPretragaIme.Text && (korisnik.Active == true)
                                          select korisnik;

            listKorisnici.ItemsSource = query;
            view.Refresh();

            if (txtPretragaIme.Text.Equals(string.Empty))
            {
                query = from korisnik in Data.Instance.Korisnici
                        orderby korisnik.Ime
                        where korisnik.Active == true
                        select korisnik;

                listKorisnici.ItemsSource = query;
                view.Refresh();
            }

        }

        private void txtPretragaPrezime_KeyUp(object sender, KeyEventArgs e)
        {

            txtPretragaIme.Text = string.Empty;
            txtPretragaKorisnickoIme.Text = string.Empty;
            IEnumerable<Korisnik> query = from korisnik in Data.Instance.Korisnici
                                          orderby korisnik.Prezime
                                          where korisnik.Prezime == txtPretragaPrezime.Text && (korisnik.Active == true)
                                          select korisnik;

            listKorisnici.ItemsSource = query;
            view.Refresh();

            if (txtPretragaPrezime.Text.Equals(string.Empty))
            {
                query = from korisnik in Data.Instance.Korisnici
                                              orderby korisnik.Ime
                                              where korisnik.Active == true
                                              select korisnik;

                listKorisnici.ItemsSource = query;
                view.Refresh();
            }

        }

        private void txtPretragaKorisnickoIme_KeyUp(object sender, KeyEventArgs e)
        {
            txtPretragaIme.Text = string.Empty;
            txtPretragaPrezime.Text = string.Empty;
            IEnumerable<Korisnik> query = from korisnik in Data.Instance.Korisnici
                                          orderby korisnik.KorisnickoIme
                                          where korisnik.KorisnickoIme == txtPretragaKorisnickoIme.Text && (korisnik.Active == true)
                                          select korisnik;

            listKorisnici.ItemsSource = query;
            view.Refresh();

            if (txtPretragaKorisnickoIme.Text.Equals(string.Empty))
            {
                query = from korisnik in Data.Instance.Korisnici
                        orderby korisnik.Ime
                        where korisnik.Active == true
                        select korisnik;

                listKorisnici.ItemsSource = query;
                view.Refresh();
            }

        }

        private void txtPonistiPretragu_Click(object sender, RoutedEventArgs e)
        {
            txtPretragaIme.Text = string.Empty;
            txtPretragaPrezime.Text = string.Empty;
            txtPretragaKorisnickoIme.Text = string.Empty;
            IEnumerable<Korisnik> query = from korisnik in Data.Instance.Korisnici
                                          where korisnik.Active == true
                                          orderby korisnik.Ime
                                          select korisnik;

            listKorisnici.ItemsSource = query;
            view.Refresh();
        }

        private void btnOpadajuce_Click(object sender, RoutedEventArgs e)
        {
            // listKorisnici.ItemsSource = Data.Instance.Korisnici.Where(k => k.Active).OrderByDescending(k => k.Ime);
            // view.Refresh();
         
            IEnumerable<Korisnik> query = from korisnik in Data.Instance.Korisnici
                                        orderby korisnik.Ime descending
                                        where korisnik.Active == true
                                        select korisnik;

            listKorisnici.ItemsSource = query;
            view.Refresh();

        }

        private void btnRastuce_Click(object sender, RoutedEventArgs e)
        {
            //listKorisnici.ItemsSource = Data.Instance.Korisnici.Where(k => k.Active).OrderBy(k => k.Ime);
            //view.Refresh();


            IEnumerable<Korisnik> query = from korisnik in Data.Instance.Korisnici
                                        orderby korisnik.Ime
                                        where korisnik.Active == true
                                        select korisnik;

            listKorisnici.ItemsSource = query;
            view.Refresh();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AvioKarte.Model;
using AvioKarte.Pages;
using AvioKarte.Database;
using AvioKarte.Utility;

namespace AvioKarte
{
    /// <summary>
    /// Interaction logic for PutnikWindow.xaml
    /// </summary>
    public partial class PutnikWindow : Window
    {
        public PutnikWindow()
        {
            
            InitializeComponent();
            lblPutnik.Content = Tool.Instance.loggedUser.KorisnickoIme;
        }

        private void btnPocetna_Click(object sender, RoutedEventArgs e)
        {
            Main.Content = new PutnikPocetnaPage();
        }

        private void btnMojProfil_Click(object sender, RoutedEventArgs e)
        {
            Main.Content = new MojProfilPutnikPage();
        }

        private void btnMojeKarte_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mw = new MainWindow();
            Tool.Instance.loggedUser = null;
            this.Close();
            mw.Show();
            
        }

        private void btnLetovi_Click(object sender, RoutedEventArgs e)
        {
            Main.Content = new LetoviPage(Main);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AvioKarte.Database;
using AvioKarte.Model;
using AvioKarte.Utility;

namespace AvioKarte
{
    /// <summary>
    /// Interaction logic for LetoviWindow.xaml
    /// </summary>
    public partial class LetoviWindow : Window
    {
        ICollectionView view;
        public LetoviWindow()
        {
            InitializeComponent();
            view = CollectionViewSource.GetDefaultView(Data.Instance.Letovi.Where(L => L.Active));
            view.Filter = Filter;
            listLetovi.IsSynchronizedWithCurrentItem = true;
            listLetovi.ItemsSource = Data.Instance.Letovi.Where(L => L.Active);
            txtPretragaCenaOd.Text = "0";
            txtPretragaCenaDo.Text = "100000";
            DatumOd.SelectedDate = DateTime.Now.AddDays(1);
            DatumDo.SelectedDate = DateTime.Now.AddDays(200);

        }

        private bool Filter(object obj)
        {
            Let let = obj as Let;
            return let.Active;
        }

        private void btnIzlaz_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDodajLet_Click(object sender, RoutedEventArgs e)
        {
            EditLetoviWindow elw = new EditLetoviWindow(new Let(), EditLetoviWindow.Opcija.DODAVANJE);
            
            if(elw.ShowDialog() == false)
            {
                IEnumerable<Let> query = from let1 in Data.Instance.Letovi
                                         orderby let1.CenaKarte
                                         where let1.Active == true
                                         select let1;

                listLetovi.ItemsSource = query;
                

                view.Refresh();
            }
        }

        private void btnIzmeniLet_Click(object sender, RoutedEventArgs e)
        {
            Let selektovaniLet = listLetovi.SelectedItem as Let;
            if(selektovaniLet != null)
            {
                Let clone = selektovaniLet.Clone() as Let;
                EditLetoviWindow elw = new EditLetoviWindow(clone, EditLetoviWindow.Opcija.IZMENA);
                if (elw.ShowDialog() == false)
                {
                    IEnumerable<Let> query = from let1 in Data.Instance.Letovi
                                             orderby let1.CenaKarte
                                             where let1.Active == true
                                             select let1;

                    listLetovi.ItemsSource = query;


                    view.Refresh();
                }
            }
            else
            {
                MessageBox.Show("Selektujte let!");
            }
            
        }

        private void btnObrisiLet_Click(object sender, RoutedEventArgs e)
        {
            Let let = listLetovi.SelectedItem as Let;
            if (let != null)
            {
                if (MessageBox.Show($"Da li zelite da obrisete let: {let}", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    let.Delete();
                    Data.Instance.UcitajLetoveDB();

                    IEnumerable<Let> query = from let1 in Data.Instance.Letovi
                                             where let1.Active == true
                                             select let1;

                    listLetovi.ItemsSource = query;
                    view.Refresh();
                }
            }
            else
            {
                MessageBox.Show("Selektujte let!");
            }
            view.Refresh();
        }

        private void btnOpadajuce_Click(object sender, RoutedEventArgs e)
        {
            IEnumerable<Let> query = from let1 in Data.Instance.Letovi
                                     orderby let1.CenaKarte descending
                                     where let1.Active == true
                                     select let1;

            listLetovi.ItemsSource = query;
            view.Refresh();
        }

        private void btnRastuce_Click(object sender, RoutedEventArgs e)
        {
            IEnumerable<Let> query = from let1 in Data.Instance.Letovi
                                          orderby let1.CenaKarte
                                          where let1.Active == true
                                          select let1;

            listLetovi.ItemsSource = query;
            view.Refresh();
        }

      

        private void btnPretrazi_Click(object sender, RoutedEventArgs e)
        {
          
            try
            {
                listLetovi.ItemsSource = Data.Instance.Letovi.Where(l => l.BrojLeta.Contains(txtPretragaBrojLeta.Text) && l.Aviokompanija.Naziv.Contains(txtPretragaAviokompanija.Text) &&
                                                            l.CenaKarte >= int.Parse(txtPretragaCenaOd.Text) && l.CenaKarte <= int.Parse(txtPretragaCenaDo.Text) &&
                                                            l.VremePolaska.Date >= DatumOd.SelectedDate.Value && l.VremeDolaska <= DatumDo.SelectedDate.Value &&
                                                            l.Polaziste.Naziv.ToLower().Contains(txtAerodromOd.Text.ToLower()) && l.Odrediste.Naziv.ToLower().Contains(txtAerodromDo.Text.ToLower()));

            }catch(Exception ex)
            {
                MessageBox.Show("Pogresan unos");
            }



        }

        private void txtPonistiPretragu_Click(object sender, RoutedEventArgs e)
        {
            txtPretragaBrojLeta.Text = string.Empty;
            txtPretragaAviokompanija.Text = string.Empty;
            txtPretragaCenaOd.Text = "0";
            txtPretragaCenaDo.Text = "100000";
            txtAerodromOd.Text = string.Empty;
            txtAerodromDo.Text = string.Empty;
            DatumOd.SelectedDate = DateTime.Now.AddDays(1);
            DatumDo.SelectedDate = DateTime.Now.AddDays(200);

            IEnumerable<Let> query = from let1 in Data.Instance.Letovi
                                     orderby let1.CenaKarte
                                     where let1.Active == true
                                     select let1;

            listLetovi.ItemsSource = query;
            view.Refresh();
        }



    }
}

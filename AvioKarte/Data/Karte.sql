﻿CREATE TABLE [dbo].[Karte]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [LetID] INT NOT NULL FOREIGN KEY REFERENCES Letovi(id), 
    [Sediste] VARCHAR(2) NULL, 
    [PutnikID] INT NULL FOREIGN KEY REFERENCES Korisnici(id), 
    [Ime] VARCHAR(50) NULL, 
    [Prezime] VARCHAR(50) NULL, 
	[Pol] VARCHAR(50) NULL, 
    [Adresa] VARCHAR(50) NULL, 
    [Email] VARCHAR(50) NULL, 
    [Active] BIT NULL
)

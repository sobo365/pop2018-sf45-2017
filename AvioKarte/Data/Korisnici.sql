﻿CREATE TABLE [dbo].[Korisnici] (
    [Id]            INT          IDENTITY (1, 1) NOT NULL,
    [Ime]           VARCHAR (50) NOT NULL,
    [Prezime]       VARCHAR (50) NOT NULL,
    [Lozinka]       VARCHAR (50) NOT NULL,
    [KorisnickoIme] VARCHAR (50) NOT NULL,
    [Pol]           VARCHAR (50) NOT NULL,
    [Adresa]        VARCHAR (50) NOT NULL,
	[Email]			VARCHAR (50) NOT NULL,
    [TipKorisnika]  VARCHAR (50) NOT NULL,
    [Active]        BIT          NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);
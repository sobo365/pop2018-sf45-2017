﻿CREATE TABLE [dbo].[Aerodromi] (
    [Id]     INT          IDENTITY (1, 1) NOT NULL,
    [Sifra]  VARCHAR (50) NOT NULL,
    [Grad]   VARCHAR (50) NULL,
    [Naziv]  VARCHAR (50) NULL,
	[BrojKapija] INT	,
    [Active] BIT          NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

insert into Aerodromi(Sifra, Grad, Naziv, BrojKapija, Active) 
values('BEG', 'Beograd', 'Belgrade Nikola Tesla Airport', 4, 1);

insert into Aerodromi(Sifra, Grad, Naziv, BrojKapija, Active) 
values('ATL', 'Atlanta, Georgia', 'Hartsfield–Jackson Atlanta International Airport', 4, 1);

insert into Aerodromi(Sifra, Grad, Naziv, BrojKapija, Active) 
values('PEK', 'Chaoyang-Shunyi, Beijing', 'Beijing Capital International Airport', 4, 1);

insert into Aerodromi(Sifra, Grad, Naziv, BrojKapija, Active) 
values('DXB', 'Garhoud, Dubai', 'Dubai International Airport', 4, 1);

insert into Aerodromi(Sifra, Grad, Naziv, BrojKapija, Active) 
values('HND', 'Ota, Tokyo', 'Tokyo Haneda Airport', 4, 1);

insert into Aerodromi(Sifra, Grad, Naziv, BrojKapija, Active) 
values('LAX', 'Los Angeles, California', 'Los Angeles International Airport', 4, 1);

insert into Aerodromi(Sifra, Grad, Naziv, BrojKapija, Active) 
values('HKG', 'Chek Lap Kok, Hong Kong', 'Hong Kong International Airport', 4, 1);

insert into Aerodromi(Sifra, Grad, Naziv, BrojKapija, Active) 
values('ORD', 'Chicago, Illinois', 'O Hare International Airport', 4, 1);

insert into Aerodromi(Sifra, Grad, Naziv, BrojKapija, Active) 
values('LDN', 'Hillingdon, London', 'London Heathrow Airport', 4, 1);
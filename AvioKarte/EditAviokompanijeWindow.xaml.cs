﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AvioKarte.Model;
using AvioKarte.Utility;
using AvioKarte.Database;



namespace AvioKarte
{
    /// <summary>
    /// Interaction logic for EditAviokompanijeWindow.xaml
    /// </summary>
    public partial class EditAviokompanijeWindow : Window
    {
        public enum Opcija { DODAVANJE, IZMENA};
        private Opcija opcija;
        private Aviokompanija aviokompanija;

        public EditAviokompanijeWindow(Aviokompanija aviokompanija, Opcija opcija)
        {
            InitializeComponent();
            this.aviokompanija = aviokompanija;
            this.opcija = opcija;
            this.DataContext = aviokompanija;

            if (opcija.Equals(Opcija.IZMENA))
            {
                txtSifra.IsEnabled = false;
             
            }
        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            /*  try
              {
                  if (aviokompanija.Sifra.Trim().Equals(string.Empty) || aviokompanija.Naziv.Trim().Equals(string.Empty))
                  {
                      if (!MessageBox.Show("Niste uneli sve podatke, da li zelite da pokusate ponovo?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                      {
                          this.Close();
                      }
                      return;
                  }
              }
              catch (Exception ex)
              {
                  if (!MessageBox.Show("Niste uneli sve podatke, da li zelite da pokusate ponovo?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                  {
                      this.Close();
                  }
                  return;
              }*/

            if (txtNaziv.Text.Equals(string.Empty) || txtSifra.Text.Equals(string.Empty))
            {
                lblErrorMessage.Content = "Niste uneli sve podatke!";
            }
            else
            {
                if (opcija.Equals(Opcija.DODAVANJE))
                {

                    if (!Tool.Instance.proveriPostojeceSifreAviokompanije(aviokompanija.Sifra.Trim()))
                    {
                        aviokompanija.Insert();
                        Data.Instance.UcitajAviokompanijeDB();
                        MessageBox.Show($"Uspesno ste dodali novu aviokompaniju: {aviokompanija}");
                        this.Close();
                    }
                    else
                    {
                        lblErrorMessage.Content = "Ova sifra se vec koristi!";
                    }
                }
                if (opcija.Equals(Opcija.IZMENA))
                {
                    aviokompanija.Update();
                    Data.Instance.UcitajAviokompanijeDB();
                    this.Close();
                }
            }  
            
        }

        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

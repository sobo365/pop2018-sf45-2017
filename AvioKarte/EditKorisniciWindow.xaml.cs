﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AvioKarte.Model;
using AvioKarte.Database;
using AvioKarte.Utility;
using System.ComponentModel;

namespace AvioKarte
{
    /// <summary>
    /// Interaction logic for EditKorisniciWindow.xaml
    /// </summary>
    public partial class EditKorisniciWindow : Window
    {
        public enum Opcija { DODAVANJE, IZMENA, KORISNIK_IZMENA};
        private Opcija opcija;
        private Korisnik korisnik;
        public EditKorisniciWindow(Korisnik korisnik, Opcija opcija)
        {
            
            InitializeComponent();
            this.korisnik = korisnik;
            this.opcija = opcija;
            DataContext = korisnik;

            cbTipKorisnika.ItemsSource = Enum.GetValues(typeof(ETipKorisnika)).Cast<ETipKorisnika>();
            cbPolKorisnika.ItemsSource = Enum.GetValues(typeof(EPol)).Cast<EPol>();
            
            if(opcija.Equals(Opcija.IZMENA))
            {
                txtKorisnickoIme.IsEnabled = false;
            }
            if (opcija.Equals(Opcija.KORISNIK_IZMENA))
            {
                cbTipKorisnika.IsEnabled = false;
                txtKorisnickoIme.IsEnabled = false;
            }



        }

        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
                if (txtKorisnickoIme.Text.Equals(string.Empty) || txtImeKorisnika.Text.Equals(string.Empty) || txtPrezimeKorisnika.Text.Equals(string.Empty) || txtLozinkaKorisnika.Text.Equals(string.Empty) || txtEmail.Text.Equals(string.Empty) || txtAdresaKorisnika.Text.Equals(string.Empty))
                {
                    lblErrorMessage.Content = "Niste uneli sve podatke!";
                }
                else
                {
                    if (opcija.Equals(Opcija.IZMENA))
                    {
                        korisnik.Update();
                        Data.Instance.UcitajKorisnikeDB();
                        this.Close();
                    }
                    if (opcija.Equals(Opcija.KORISNIK_IZMENA))
                    {
                        korisnik.Update();
                        Data.Instance.UcitajKorisnikeDB();
                        Tool.Instance.loggedUser = korisnik;
                        this.Close();
                    }
                    if (opcija.Equals(Opcija.DODAVANJE))
                    {
                        if (!Tool.Instance.proveriPostojeceKorisnickoIme(korisnik.KorisnickoIme.Trim()))
                        {
                            //Data.Instance.Korisnici.Add(korisnik);
                            korisnik.Insert();
                            MessageBox.Show($"Uspesno ste dodali korisnika: {korisnik}");
                            Data.Instance.UcitajKorisnikeDB();
                            this.Close();
                        }
                        else
                        {
                            lblErrorMessage.Content = "Ovo korisnicko ime se vec koristi!";
                        }
                    }
                }
            

        }
    }
}
